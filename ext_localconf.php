<?php
defined('TYPO3_MODE') or die();


call_user_func(
    function ()
{

    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'Yawave.Yawave',
        'Yawave',
        [
            'Publications' => 'list,update,detail,portals',
            'Update' => 'create,edit,update',
            'LiveTicker' => 'liveticker,livetickerDetail'
        ],
        // non-cacheable actions
        [
            'Publications' => 'list,update,detail,portals',
            'Update' => 'create,edit,update',
            'LiveTicker' => 'liveticker,livetickerDetail'
        ]
    );


    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'Yawave.Yawave',
        'YawaveHook',
        [
            'Publications' => 'pushNotification',
            'Update' => 'create,edit,update'
        ],
        // non-cacheable actions
        [
            'Publications' => 'pushNotification',
            'Update' => 'create,edit,update'
        ]
    );

    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'Yawave.Yawave',
        'LiveTickerHook',
        [
            'LiveTicker' => 'pushNotification',

        ],
        // non-cacheable actions
        [
            'LiveTicker' => 'pushNotification',
        ]
    );


    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        'Yawave.Yawave',
        'liveTickerBlogPost',
        [
            'Publications' => 'livtickerAjax',

        ],
        // non-cacheable actions
        [
            'Publications' => 'livtickerAjax',
        ]
    );

//    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
//        'Yawave.Yawave',
//        'individualLiveTickerBlog',
//        [
//            'Publications' => 'livetickerDetail',
//
//        ],
//        // non-cacheable actions
//        [
//            'Publications' => 'livetickerDetail',
//        ]
//    );


    $GLOBALS['TYPO3_CONF_VARS']['FE']['pageNotFoundOnCHashError'] = false;

    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['scheduler']['tasks'][\Yawave\Yawave\Task\InportYawaveRecords::class] = [
        'extension' => 'yawave',
        'title' => 'LLL:EXT:yawave/Resources/Private/Language/locallang.xlf:task.import_publication.import_title',
        'description' => 'LLL:EXT:yawave/Resources/Private/Language/locallang.xlf:task.import_publication.import_description'
    ];

});




$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['news']['controller']['News']['nonCacheableActions'][] = 'list';
$GLOBALS['TYPO3_CONF_VARS']['SYS']['Objects'][GeorgRinger\News\Controller\NewsController::class] = [
    'className' => Yawave\Yawave\Controller\PublicationsController::class
];

$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS'][\TYPO3\CMS\Core\Configuration\FlexForm\FlexFormTools::class]['flexParsing'][] = \Yawave\Yawave\Hooks\BackendUtility::class;
