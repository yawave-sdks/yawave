<?php


namespace Yawave\Yawave\Domain\Model;

/***
 *
 * This file is part of the "Yawave Typo3 Extension" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) Epic Software Solutions <info@epicss.io>, Epic Software Solutions
 *
 ***/
/**
 * Sources
 */


class Sources extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * type
     *
     * @var string
     */
    protected $type = '';

    /**
     * type
     *
     * @var string
     */
    protected $sportEventId = '';

    /**
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Yawave\Yawave\Domain\Model\Competition>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Lazy
     */
    protected $competition;


    /**
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Yawave\Yawave\Domain\Model\HomeCompetitorExternal>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Lazy
     */
    protected $homeCompetitorExternal;

    /**
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Yawave\Yawave\Domain\Model\AwayCompetitorExternal>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Lazy
     */
    protected $awayCompetitorExternal;


    /**
     * __construct
     */
    public function __construct()
    {

        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }

    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     *
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->competition = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->homeCompetitorExternal = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->awayCompetitorExternal = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();

    }




    /**
     * Returns the type
     *
     * @return string $type
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Sets the type
     *
     * @param string $type
     * @return void
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * Returns the sportEventId
     *
     * @return string $sportEventId
     */
    public function getSportEventId()
    {
        return $this->sportEventId;
    }

    /**
     * Sets the sportEventId
     *
     * @param string $sportEventId
     * @return void
     */
    public function setSportEventId($sportEventId)
    {
        $this->sportEventId = $sportEventId;
    }


    /**
     * Adds a Competition
     *
     * @param \Yawave\Yawave\Domain\Model\Competition $competition
     * @return void
     */
    public function addTool(\Yawave\Yawave\Domain\Model\Competition $competition)
    {
        $this->competition->attach($competition);
    }

    /**
     * Removes a Competition
     *
     * @param \Yawave\Yawave\Domain\Model\Competition $competitionToRemove The Tools to be removed
     * @return void
     */
    public function removeTool(\Yawave\Yawave\Domain\Model\Competition $competitionToRemove)
    {
        $this->competition->detach($competitionToRemove);
    }

    /**
     * Returns the Competition
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Yawave\Yawave\Domain\Model\Competition> $competition
     */
    public function getCompetition()
    {
        return $this->competition;
    }

    /**
     * Sets the Competitionw
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Yawave\Yawave\Domain\Model\Competition> $competition
     * @return void
     */
    public function setCompetition(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $competition)
    {
        $this->competition = $competition;
    }


    /**
     * Adds a HomeCompetitorExternal
     *
     * @param \Yawave\Yawave\Domain\Model\HomeCompetitorExternal $homeCompetitorExternal
     * @return void
     */
    public function addHomeCompetitorExternal(\Yawave\Yawave\Domain\Model\HomeCompetitorExternal $homeCompetitorExternal)
    {
        $this->homeCompetitorExternal->attach($homeCompetitorExternal);
    }

    /**
     * Removes a HomeCompetitorExternal
     *
     * @param \Yawave\Yawave\Domain\Model\HomeCompetitorExternal $homeCompetitorExternalToRemove The HomeCompetitorExternal to be removed
     * @return void
     */
    public function removeHomeCompetitorExternal(\Yawave\Yawave\Domain\Model\HomeCompetitorExternal $homeCompetitorExternalToRemove)
    {
        $this->homeCompetitorExternal->detach($homeCompetitorExternalToRemove);
    }

    /**
     * Returns the HomeCompetitorExternal
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Yawave\Yawave\Domain\Model\HomeCompetitorExternal> $homeCompetitorExternal
     */
    public function getHomeCompetitorExternal()
    {
        return $this->homeCompetitorExternal;
    }

    /**
     * Sets the HomeCompetitorExternal
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Yawave\Yawave\Domain\Model\HomeCompetitorExternal> $homeCompetitorExternal
     * @return void
     */
    public function setHomeCompetitorExternal(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $homeCompetitorExternal)
    {
        $this->homeCompetitorExternal = $homeCompetitorExternal;
    }



    /**
     * Adds a AwayCompetitorExternal
     *
     * @param \Yawave\Yawave\Domain\Model\AwayCompetitorExternal $awayCompetitorExternal
     * @return void
     */
    public function addAwayCompetitorExternal(\Yawave\Yawave\Domain\Model\AwayCompetitorExternal $awayCompetitorExternal)
    {
        $this->awayCompetitorExternal->attach($awayCompetitorExternal);
    }

    /**
     * Removes a AwayCompetitorExternal
     *
     * @param \Yawave\Yawave\Domain\Model\AwayCompetitorExternal $awayCompetitorExternal The AwayCompetitorExternal to be removed
     * @return void
     */
    public function removeAwayCompetitorExternal(\Yawave\Yawave\Domain\Model\AwayCompetitorExternal $awayCompetitorExternal)
    {
        $this->awayCompetitorExternal->detach($awayCompetitorExternal);
    }

    /**
     * Returns the AwayCompetitorExternal
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Yawave\Yawave\Domain\Model\AwayCompetitorExternal> $awayCompetitorExternal
     */
    public function getAwayCompetitorExternal()
    {
        return $this->awayCompetitorExternal;
    }

    /**
     * Sets the AwayCompetitorExternal
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Yawave\Yawave\Domain\Model\AwayCompetitorExternal> $awayCompetitorExternal
     * @return void
     */
    public function setAwayCompetitorExternal(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $awayCompetitorExternal)
    {
        $this->awayCompetitorExternal = $awayCompetitorExternal;
    }


}