<?php


namespace Yawave\Yawave\Domain\Model;

/***
 *
 * This file is part of the "Yawave Typo3 Extension" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) Epic Software Solutions <info@epicss.io>, Epic Software Solutions
 *
 ***/
/**
 * Update
 */
class Livtickerpostupdate extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * eventType
     *
     * @var string
     */
    protected $eventType = '';

    /**
     * applicationUuid
     *
     * @var string
     */
    protected $applicationUuid = '';

    /**
     * liveblogUuid
     *
     * @var string
     */
    protected $liveblogUuid = '';

    /**
     * liveblogPostUuid
     *
     * @var string
     */
    protected $liveblogPostUuid = '';

    /**
     * Returns the eventType
     *
     * @return string $eventType
     */
    public function getEventType()
    {
        return $this->eventType;
    }

    /**
     * Sets the eventType
     *
     * @param string $eventType
     * @return void
     */
    public function setEventType($eventType)
    {
        $this->eventType = $eventType;
    }

    /**
     * Returns the applicationUuid
     *
     * @return string $applicationUuid
     */
    public function getApplicationUuid()
    {
        return $this->applicationUuid;
    }

    /**
     * Sets the applicationUuid
     *
     * @param string $applicationUuid
     * @return void
     */
    public function setApplicationUuid($applicationUuid)
    {
        $this->applicationUuid = $applicationUuid;
    }

    /**
     * Returns the liveblogUuid
     *
     * @return string $liveblogUuid
     */
    public function getLiveblogUuid()
    {
        return $this->liveblogUuid;
    }

    /**
     * Sets the liveblogUuid
     *
     * @param string $liveblogUuid
     * @return void
     */
    public function setLiveblogUuid($liveblogUuid)
    {
        $this->liveblogUuid = $liveblogUuid;
    }

    /**
     * Returns the liveblogPostUuid
     *
     * @return string $liveblogPostUuid
     */
    public function getLiveblogPostUuid()
    {
        return $this->liveblogPostUuid;
    }

    /**
     * Sets the liveblogPostUuid
     *
     * @param string $liveblogPostUuid
     * @return void
     */
    public function setLiveblogPostUuid($liveblogPostUuid)
    {
        $this->liveblogPostUuid = $liveblogPostUuid;
    }
}

