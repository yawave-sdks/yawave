<?php


namespace Yawave\Yawave\Domain\Model;

/***
 *
 * This file is part of the "Yawave Typo3 Extension" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) Epic Software Solutions <info@epicss.io>, Epic Software Solutions
 *
 ***/
/**
 * AwayCompetitor
 */

class AwayCompetitor extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * name
     *
     * @var string
     */
    protected $name = '';

    /**
     * images
     *
     * @var \Yawave\Yawave\Domain\Model\Images
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $images = null;

    /**
     * Returns the Name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets the Name
     *
     * @param string $name
     * @return void
     */
    public function setName($name)
    {
        $this->name = $name;
    }


    /**
     * Returns the images
     *
     * @return \Yawave\Yawave\Domain\Model\Images $images
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * Sets the images
     *
     * @param \Yawave\Yawave\Domain\Model\Images $images
     * @return void
     */
    public function setImages(\Yawave\Yawave\Domain\Model\Images $images)
    {
        $this->images = $images;
    }

}