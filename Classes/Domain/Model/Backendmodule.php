<?php

namespace Yawave\Yawave\Domain\Model;


use TYPO3\CMS\Core\Utility\GeneralUtility;

/***
 *
 * This file is part of the "Yawave Typo3 Extension" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) Epic Software Solutions <info@epicss.io>, Epic Software Solutions
 *
 ***/
/**
 * Covers
 */
class Backendmodule extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * hooktoken
     *
     * @var string
     */
    protected $hooktoken = '';

    /**
     * storagePid
     *
     * @var string
     */
    protected $storagePid = '';

    /**
     * clientId
     *
     * @var string
     */
    protected $clientId = '';

    /**
     * secret
     *
     * @var string
     */
    protected $secret = '';

    /**
     * apiDomain
     *
     * @var string
     */
    protected $apiDomain = '';

    /**
     * languageIds
     *
     * @var string
     */
    protected $languageIds;


    /**
     * initializes this object
     *
     * @param string $hooktoken
     * @param string $storagePid
     * @param string $clientId
     * @param string $secret
     * @param string $apiDomain
     * @param array $languageIds
     */
    public function __construct($hooktoken = '', $storagePid='', $clientId = '', $secret = '', $apiDomain = '', array $languageIds = array()) {
        $this->setHooktoken($hooktoken);
        $this->setstoragePid($storagePid);
        $this->setClientId($clientId);
        $this->setSecret($secret);
        $this->setApiDomain($apiDomain);
        $this->setLanguageIds($languageIds);
    }


    /**
     * Returns the hooktoken
     *
     * @return string $hooktoken
     */
    public function getHooktoken()
    {
        return $this->hooktoken;
    }

    /**
     * Sets the hooktoken
     *
     * @param string $hooktoken
     * @return void
     */
    public function setHooktoken($hooktoken)
    {
        $this->hooktoken = $hooktoken;
    }



    /**
     * Returns the storagePid
     *
     * @return string $storagePid
     */
    public function getStoragePid()
    {
        return $this->storagePid;
    }

    /**
     * Sets the storagePid
     *
     * @param string $storagePid
     * @return void
     */
    public function setStoragePid($storagePid)
    {
        $this->storagePid = $storagePid;
    }


    /**
     * Returns the clientId
     *
     * @return string $clientId
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * Sets the source
     *
     * @param string $clientId
     * @return void
     */
    public function setClientId($clientId)
    {
        $this->clientId = $clientId;
    }


    /**
     * Returns the secret
     *
     * @return string $secret
     */
    public function getSecret()
    {
        return $this->secret;
    }

    /**
     * Sets the secret
     *
     * @param string $secret
     * @return void
     */
    public function setSecret($secret)
    {
        $this->secret = $secret;
    }


    /**
     * Returns the apiDomain
     *
     * @return string $apiDomain
     */
    public function getApiDomain()
    {
        return $this->apiDomain;
    }

    /**
     * Sets the apiDomain
     *
     * @param string $apiDomain
     * @return void
     */
    public function setApiDomain($apiDomain)
    {
        $this->apiDomain = $apiDomain;
    }


    /**
     * Returns the languageIds
     *
     * @return array $languageIds
     */
    public function getLanguageIds()
    {
        if($this->languageIds !== '') {
            $sitesConfigurations = GeneralUtility::makeInstance(\TYPO3\CMS\Core\Site\SiteFinder::class)->getAllSites();
            $langugeToArray = array_map('intval', explode(',', $this->languageIds));

            $sortedLanguage = [];
            foreach ($sitesConfigurations as $siteConfiguration) {
                $languages = $siteConfiguration->getLanguages();
                foreach ($languages as $language) {
                    $isoCode = $language->getTwoLetterIsoCode();
                    $languageId = $language->getLanguageId();
                    $sortedLanguage[$isoCode] = $languageId;
                }

            }

            $languages = array_intersect($sortedLanguage, $langugeToArray);
            return $languages;
        }

        return $this->languageIds;
    }

    /**
     * Sets the languageIds
     *
     * @param array $languageIds
     * @return void
     */
    public function setLanguageIds(array $languageIds)
    {

        $this->languageIds = implode(',',$languageIds);
    }


}