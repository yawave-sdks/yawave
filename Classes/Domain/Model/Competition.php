<?php


namespace Yawave\Yawave\Domain\Model;

/***
 *
 * This file is part of the "Yawave Typo3 Extension" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) Epic Software Solutions <info@epicss.io>, Epic Software Solutions
 *
 ***/
/**
 * Competition
 */

class Competition extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * externalId
     *
     * @var string
     */
    protected $externalId = '';

    /**
     * name
     *
     * @var string
     */
    protected $name = '';



    /**
     * Returns the external Id
     *
     * @return string $externalId
     */
    public function getExternalId()
    {
        return $this->externalId;
    }

    /**
     * Sets the external Id
     *
     * @param string $externalId
     * @return void
     */
    public function setExternalId($externalId)
    {
        $this->externalId = $externalId;
    }

    /**
     * Returns the Name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets the Name
     *
     * @param string $name
     * @return void
     */
    public function setName($name)
    {
        $this->name = $name;
    }



}