<?php


namespace Yawave\Yawave\Domain\Model;

/***
 *
 * This file is part of the "Yawave Typo3 Extension" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) Epic Software Solutions <info@epicss.io>, Epic Software Solutions
 *
 ***/
/**
 * Liveblogpost
 */
class Liveblogpost extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * postId
     *
     * @var string
     */
    protected $postId = '';

    /**
     * source
     *
     * @var string
     */
    protected $source = '';

    /**
     * periods
     *
     * @var string
     */
    protected $periods = '';

    /**
     * minute
     *
     * @var int
     */
    protected $minute;

    /**
     * status
     *
     * @var int
     */
    protected $status;

    /**
     * stoppageTime
     *
     * @var int
     */
    protected $stoppageTime;

    /**
     * tstamp
     *
     * @var int
     */
    protected $tstamp;

    /**
     * title
     *
     * @var string
     */
    protected $title = '';

    /**
     * text
     *
     * @var string
     */
    protected $text = '';

    /**
     * url
     *
     * @var string
     */
    protected $url = '';

    /**
     * publicationId
     *
     * @var \Yawave\Yawave\Domain\Model\Publications
     *
     */
    protected $publicationId = null;

    /**
     * embedCode
     *
     * @var string
     */
    protected $embedCode = '';

    /**
     * pinned
     *
     * @var bool
     */
    protected $pinned = false;


    /**
     * creationDate
     *
     * @var \DateTime
     */
    protected $creationDate;

    /**
     * timelineTimestamp
     *
     * @var \DateTime
     */
    protected $timelineTimestamp;

    /**
     * actionId
     *
     * @var string
     */
    protected $actionId = '';

    /**
     * personId
     *
     * @var string
     */
    protected $personId = '';

    /**
     * action
     *
     * @var string
     */
    protected $action = '';

    /**
     * person
     *
     * @var string
     */
    protected $person = '';

    /**
     * externalId
     *
     * @var string
     */
    protected $externalId = '';

    /**
     * type
     *
     * @var string
     */
    protected $type = '';


    /**
     * matchClock
     *
     * @var string
     */
    protected $matchClock = '';


    /**
     * Competitor
     *
     * @var \Yawave\Yawave\Domain\Model\Competitor
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $competitor = null;

    /**
     * players
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Yawave\Yawave\Domain\Model\Players>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $players = null;

    /**
     * homeScore
     *
     * @var int
     *
     */
    protected $homeScore = 0;

    /**
     * awayScore
     *
     * @var int
     *
     */
    protected $awayScore = 0;

    /**
     * injuryTime
     *
     * @var int
     *
     */
    protected $injuryTime;


    /**
     * @var \Yawave\Yawave\Domain\Model\Liveblog
     *
     */
    protected $blog;


    /**
     * __construct
     */
    public function __construct()
    {

        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }

    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     *
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->players = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }


    /**
     * Returns the postId
     *
     * @return string $postId
     */
    public function getPostId()
    {
        return $this->postId;
    }

    /**
     * Sets the postId
     *
     * @param string $postId
     * @return void
     */
    public function setPostId($postId)
    {
        $this->postId = $postId;
    }

    /**
     * Returns the source
     *
     * @return string $source
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * Sets the postId
     *
     * @param string $source
     * @return void
     */
    public function setSource($source)
    {
        $this->source = $source;
    }


    /**
     * Returns the periods
     *
     * @return string $periods
     */
    public function getPeriods()
    {
        return $this->periods;
    }

    /**
     * Sets the periods
     *
     * @param string $periods
     * @return void
     */
    public function setPeriods($periods)
    {
        $this->periods = $periods;
    }

    /**
     * Returns the minute
     *
     * @return int $minute
     */
    public function getMinute()
    {
        return $this->minute;
    }

    /**
     * Sets the minute
     *
     * @param int $minute
     *
     */
    public function setMinute($minute)
    {
        $this->minute = $minute;
    }

    /**
     * Returns the status
     *
     * @return int $status
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Sets the status
     *
     * @param int $status
     *
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }


    /**
     * Returns the StoppageTime
     *
     * @return int $stoppageTime
     */
    public function getStoppageTime()
    {
        return $this->stoppageTime;
    }

    /**
     * Sets the StoppageTime
     *
     * @param int $stoppageTime
     *
     */
    public function setStoppageTime($stoppageTime)
    {
        $this->stoppageTime = $stoppageTime;
    }

    /**
     * @return int $tstamp
     */
    public function getTstamp() {
        return $this->tstamp;
    }

    /**
     * Returns the title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     *
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }


    /**
     * Returns the text
     *
     * @return string $text
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Sets the text
     *
     * @param string $text
     * @return void
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * Returns the url
     *
     * @return string $url
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Sets the url
     *
     * @param string $url
     * @return void
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * Returns the publicationId
     *
     * @param \Yawave\Yawave\Domain\Model\Publications $publicationId
     */
    public function getPublicationId()
    {
        return $this->publicationId;
    }

    /**
     * Sets the publicationId
     *
     * @param \Yawave\Yawave\Domain\Model\Publications $publicationId
     * @return void
     */
    public function setPublicationId(\Yawave\Yawave\Domain\Model\Publications $publicationId)
    {
        $this->publicationId = $publicationId;
    }

    /**
     * Returns the embedCode
     *
     * @return string $embedCode
     */
    public function getEmbedCode()
    {
        return $this->embedCode;
    }

    /**
     * Sets the publicationId
     *
     * @param string $embedCode
     * @return void
     */
    public function setEmbedCode($embedCode)
    {
        $this->embedCode = $embedCode;
    }


    /**
     * Returns the pinned
     *
     * @return bool $pinned
     */
    public function getPinned()
    {
        return $this->publicationId;
    }

    /**
     * Sets the pinned
     *
     * @param bool $pinned
     * @return void
     */
    public function setPinned($pinned)
    {
        $this->pinned = $pinned;
    }

    /**
     * Returns the creationDate
     *
     * @return \DateTime $creationDate
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * Sets the creationDate
     *
     * @param \DateTime $creationDate
     *
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;
    }

    /**
     * Returns the timelineTimestamp
     *
     * @return \DateTime $timelineTimestamp
     */
    public function getTimelineTimestamp()
    {
        return $this->timelineTimestamp;
    }

    /**
     * Sets the timelineTimestamp
     *
     * @param \DateTime $timelineTimestamp
     *
     */
    public function setTimelineTimestamp($timelineTimestamp)
    {
        $this->timelineTimestamp = $timelineTimestamp;
    }

    /**
     * Returns the actionId
     *
     * @return string $actionId
     */
    public function getActionId()
    {
        return $this->actionId;
    }

    /**
     * Sets the actionId
     *
     * @param string $actionId
     * @return void
     */
    public function setActionId($actionId)
    {
        $this->actionId = $actionId;
    }


    /**
     * Returns the personId
     *
     * @return string $personId
     */
    public function getPersonId()
    {
        return $this->personId;
    }

    /**
     * Sets the personId
     *
     * @param string $personId
     * @return void
     */
    public function setPersonId($personId)
    {
        $this->personId = $personId;
    }

    /**
     * Returns the action
     *
     * @return string $action
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * Sets the action
     *
     * @param string $action
     * @return void
     */
    public function setAction($action)
    {
        $this->action = $action;
    }

    /**
     * Returns the person
     *
     * @return string $person
     */
    public function getPerson()
    {
        return $this->person;
    }

    /**
     * Sets the person
     *
     * @param string $person
     * @return void
     */
    public function setPerson($person)
    {
        $this->person = $person;
    }

    /**
     * Returns the externalId
     *
     * @return string $externalId
     */
    public function getExternalId()
    {
        return $this->externalId;
    }

    /**
     * Sets the externalId
     *
     * @param string $externalId
     * @return void
     */
    public function setExternalId($externalId)
    {
        $this->externalId = $externalId;
    }


    /**
     * Returns the type
     *
     * @return string $type
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Sets the type
     *
     * @param string $type
     * @return void
     */
    public function setType($type)
    {
        $this->type = $type;
    }


    /**
     * Returns the matchClock
     *
     * @return string $matchClock
     */
    public function getMatchClock()
    {
        return $this->matchClock;
    }

    /**
     * Sets the matchClock
     *
     * @param string $matchClock
     * @return void
     */
    public function setMatchClock($matchClock)
    {
        $this->matchClock = $matchClock;
    }

    /**
     * Returns the Competitor
     *
     * @return \Yawave\Yawave\Domain\Model\Competitor $competitor
     */
    public function getCompetitor()
    {
        return $this->competitor;
    }

    /**
     * Sets the Competitor
     *
     * @param \Yawave\Yawave\Domain\Model\Competitor $competitor
     * @return void
     */
    public function setCompetitor(\Yawave\Yawave\Domain\Model\Competitor $competitor)
    {
        $this->competitor = $competitor;
    }

    /**
     * Returns the players
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Yawave\Yawave\Domain\Model\Players> Players
     */
    public function getPlayers()
    {
        return $this->players;
    }

    /**
     * Sets the publications
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Yawave\Yawave\Domain\Model\Players> $players
     * @return void
     */
    public function setPlayers(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $players)
    {
        $this->players = $players;
    }

    /**
     * Adds a publications
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Yawave\Yawave\Domain\Model\Players> $players
     * @return void
     */
    public function addPlayers(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $players)
    {
        $this->players->attach($players);
    }


    /**
     * Returns the homeScore
     *
     * @return int $homeScore
     */
    public function getHomeScore()
    {
        return $this->homeScore;
    }

    /**
     * Sets the homeScore
     *
     * @param int $homeScore
     *
     */
    public function setHomeScore($homeScore)
    {
        $this->homeScore = $homeScore;
    }

    /**
     * Returns the awayScore
     *
     * @return int $awayScore
     */
    public function getAwayScore()
    {
        return $this->awayScore;
    }

    /**
     * Sets the awayScore
     *
     * @param int $awayScore
     *
     */
    public function setAwayScore($awayScore)
    {
        $this->awayScore = $awayScore;
    }

    /**
     * Returns the injuryTime
     *
     * @return int $injuryTime
     */
    public function getInjuryTime()
    {
        return $this->injuryTime;
    }

    /**
     * Sets the injuryTime
     *
     * @param int $injuryTime
     *
     */
    public function setInjuryTime($injuryTime)
    {
        $this->injuryTime = $injuryTime;
    }

    /**
     * Get Blog
     *
     * @return \Yawave\Yawave\Domain\Model\Liveblog Blog
     */
    public function getBlog()
    {
        return $this->blog;
    }

    /**
     * Set Blog
     *
     * @param \Yawave\Yawave\Domain\Model\Liveblog $blog
     */
    public function setBlog($blog)
    {
        $this->blog = $blog;
    }

    /**
     * Adds a Post
     *
     * @param \Yawave\Yawave\Domain\Model\Liveblog $blog
     */
    public function addBlog(\Yawave\Yawave\Domain\Model\Liveblog $blog)
    {
        $this->blog->attach($blog);
    }

    /**
     * Removes a Post
     *
     * @param \Yawave\Yawave\Domain\Model\Liveblog $blog
     */
    public function removeBlog(\Yawave\Yawave\Domain\Model\Liveblog $blog)
    {
        $this->blog->detach($blog);
    }


}