<?php


namespace Yawave\Yawave\Domain\Model;


/***
 *
 * This file is part of the "Yawave Typo3 Extension" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) Epic Software Solutions <info@epicss.io>, Epic Software Solutions
 *
 ***/
/**
 * Liveblog
 */

class Liveblog extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * liveblogId
     *
     * @var string
     */
    protected $liveblogId = '';

    /**
     * title
     *
     * @var string
     */
    protected $title = '';

    /**
     * description
     *
     * @var string
     */
    protected $description = '';

    /**
     * image
     *
     * @var \Yawave\Yawave\Domain\Model\Images
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $images = null;

    /**
     * liveblogCategoryId
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Yawave\Yawave\Domain\Model\Category>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Lazy
     */
    protected $liveblogCategoryId = null;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\GeorgRinger\News\Domain\Model\Tag>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Lazy
     */
    protected $yawaveTagId;

    /**
     * type
     *
     * @var string
     */
    protected $type = '';

    /**
     * status
     *
     * @var string
     */
    protected $status = '';

    /**
     * location
     *
     * @var string
     */
    protected $location = '';

    /**
     * startDate
     *
     * @var \DateTime
     */
    protected $startDate;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Yawave\Yawave\Domain\Model\HomeCompetitor>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Lazy
     */
    protected $homeCompetitor;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Yawave\Yawave\Domain\Model\AwayCompetitor>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Lazy
     */
    protected $awayCompetitor;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Yawave\Yawave\Domain\Model\Sources>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Lazy
     */
    protected $sources;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Yawave\Yawave\Domain\Model\Liveblogpost>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Lazy
     */
    protected $post;

    /**
     * type
     *
     * @var string
     */
    protected $sportEventId = '';

    /**
     * __construct
     */
    public function __construct()
    {

        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }

    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     *
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->liveblogCategoryId = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }


    /**
     * Returns the liveblogId
     *
     * @return string $liveblogId
     */
    public function getLiveblogId()
    {
        return $this->liveblogId;
    }

    /**
     * Sets the liveblogId
     *
     * @param string $liveblogId
     * @return void
     */
    public function setLiveblogId($liveblogId)
    {
        $this->liveblogId = $liveblogId;
    }

    /**
     * Returns the title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     *
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Returns the description
     *
     * @return string $description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Sets the description
     *
     * @param string $description
     * @return void
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }


    /**
     * Returns the images
     *
     * @return \Yawave\Yawave\Domain\Model\Images $images
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * Sets the image
     *
     * @param \Yawave\Yawave\Domain\Model\Images $images
     * @return void
     */
    public function setImages(\Yawave\Yawave\Domain\Model\Images $images)
    {
        $this->images = $images;
    }


    /**
     * Get liveblogCategoryId
     *
     * @return \Yawave\Yawave\Domain\Model\Category[]
     */
    public function getLiveblogCategoryId()
    {
        return $this->liveblogCategoryId;
    }


    /**
     * Set liveblogCategoryId
     *
     * @param  \TYPO3\CMS\Extbase\Persistence\ObjectStorage $liveblogCategoryId
     */
    public function setLiveblogCategoryId($liveblogCategoryId)
    {
        $this->liveblogCategoryId = $liveblogCategoryId;
    }

    /**
     * Adds a category to this liveblogCategoryId.
     *
     * @param Category $liveblogCategoryId
     */
    public function addLiveblogCategoryId(Category $liveblogCategoryId)
    {
        $this->getLiveblogCategoryId()->attach($liveblogCategoryId);
    }

    /**
     * Get Tags
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage
     */
    public function getYawaveTagId()
    {
        return $this->yawaveTagId;
    }

    /**
     * Set yawaveTagId
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage $yawaveTagId tags
     */
    public function setYawaveTagId($yawaveTagId)
    {
        $this->yawaveTagId = $yawaveTagId;
    }

    /**
     * Adds a yawaveTagId
     *
     * @param \GeorgRinger\News\Domain\Model\Tag $yawaveTagId
     */
    public function addYawaveTagId(\GeorgRinger\News\Domain\Model\Tag $yawaveTagId)
    {
        $this->yawaveTagId->attach($yawaveTagId);
    }

    /**
     * Removes a yawaveTagId
     *
     * @param \GeorgRinger\News\Domain\Model\Tag $yawaveTagId
     */
    public function removeYawaveTagId(\GeorgRinger\News\Domain\Model\Tag $yawaveTagId)
    {
        $this->yawaveTagId->detach($yawaveTagId);
    }


    /**
     * Returns the type
     *
     * @return string $type
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Sets the type
     *
     * @param string $type
     * @return void
     */
    public function setType($type)
    {
        $this->type = $type;
    }


    /**
     * Returns the status
     *
     * @return string $status
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Sets the status
     *
     * @param string $status
     * @return void
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }


    /**
     * Returns the location
     *
     * @return string $location
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Sets the location
     *
     * @param string $location
     * @return void
     */
    public function setLocation($location)
    {
        $this->location = $location;
    }


    /**
     * Get startDate
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set startDate time
     *
     * @param \DateTime $startDate startDate time
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;
    }


    /**
     * Get homeCompetitor
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage
     */
    public function getHomeCompetitor()
    {
        return $this->homeCompetitor;
    }

    /**
     * Set homeCompetitor
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage $homeCompetitor homeCompetitor
     */
    public function setHomeCompetitor($homeCompetitor)
    {
        $this->homeCompetitor = $homeCompetitor;
    }

    /**
     * Adds a homeCompetitor
     *
     * @param \Yawave\Yawave\Domain\Model\HomeCompetitor $homeCompetitor
     */
    public function addHomeCompetitor(\Yawave\Yawave\Domain\Model\HomeCompetitor $homeCompetitor)
    {
        $this->homeCompetitor->attach($homeCompetitor);
    }

    /**
     * Removes a homeCompetitor
     *
     * @param \Yawave\Yawave\Domain\Model\HomeCompetitor $homeCompetitor
     */
    public function removeHomeCompetitor(\Yawave\Yawave\Domain\Model\HomeCompetitor $homeCompetitor)
    {
        $this->homeCompetitor->detach($homeCompetitor);
    }



    /**
     * Get awayCompetitor
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage
     */
    public function getAwayCompetitor()
    {
        return $this->awayCompetitor;
    }

    /**
     * Set awayCompetitor
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage $awayCompetitor awayCompetitor
     */
    public function setAwayCompetitor($awayCompetitor)
    {
        $this->awayCompetitor = $awayCompetitor;
    }

    /**
     * Adds a awayCompetitor
     *
     * @param \Yawave\Yawave\Domain\Model\AwayCompetitor $awayCompetitor
     */
    public function addAwayCompetitor(\Yawave\Yawave\Domain\Model\HomeCompetitor $awayCompetitor)
    {
        $this->awayCompetitor->attach($awayCompetitor);
    }

    /**
     * Removes a awayCompetitor
     *
     * @param \Yawave\Yawave\Domain\Model\AwayCompetitor $awayCompetitor
     */
    public function removeAwayCompetitor(\Yawave\Yawave\Domain\Model\HomeCompetitor $awayCompetitor)
    {
        $this->awayCompetitor->detach($awayCompetitor);
    }


    /**
     * Get sources
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage
     */
    public function getSources()
    {
        return $this->sources;
    }

    /**
     * Set sources
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage $sources sources
     */
    public function setSources($sources)
    {
        $this->sources = $sources;
    }

    /**
     * Adds a sources
     *
     * @param \Yawave\Yawave\Domain\Model\Sources $sources
     */
    public function addSources(\Yawave\Yawave\Domain\Model\Sources $sources)
    {
        $this->sources->attach($sources);
    }

    /**
     * Removes a sources
     *
     * @param \Yawave\Yawave\Domain\Model\Sources $sources
     */
    public function removeSources(\Yawave\Yawave\Domain\Model\Sources $sources)
    {
        $this->sources->detach($sources);
    }


    /**
     * Get Post
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage
     */
    public function getPost()
    {
        return $this->post;
    }

    /**
     * Set sources
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage $post post
     */
    public function setPost($post)
    {
        $this->post = $post;
    }

    /**
     * Adds a Post
     *
     * @param \Yawave\Yawave\Domain\Model\Liveblogpost $post
     */
    public function addPost(\Yawave\Yawave\Domain\Model\Liveblogpost $post)
    {
        $this->post->attach($post);
    }

    /**
     * Removes a Post
     *
     * @param \Yawave\Yawave\Domain\Model\Liveblogpost $post
     */
    public function removePost(\Yawave\Yawave\Domain\Model\Liveblogpost $post)
    {
        $this->post->detach($post);
    }

    /**
     * Returns the sportEventId
     *
     * @return string $sportEventId
     */
    public function getSportEventId()
    {
        return $this->sportEventId;
    }

    /**
     * Sets the sportEventId
     *
     * @param string $sportEventId
     * @return void
     */
    public function setSportEventId($sportEventId)
    {
        $this->sportEventId = $sportEventId;
    }


}