<?php


namespace Yawave\Yawave\Domain\Repository;

use Yawave\Yawave\Domain\Model\HomeCompetitor;
use TYPO3\CMS\Extbase\Annotation\Inject;

class HomeCompetitorRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{

    /**
     * ImagesRepository
     *
     * @var \Yawave\Yawave\Domain\Repository\ImagesRepository
     * @Inject
     */
    protected $imagesRepository = NULL;

    public function create($homeCompetitor){
        $persistenceManager = $this->objectManager->get(\TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager::class);
        $newHomeCompetitor = new HomeCompetitor();
        $newHomeCompetitor->setName($homeCompetitor['name']);

        if($homeCompetitor['image'] !== NULL) {
            $image = [
                'image_url'=>$homeCompetitor['image']['path'],
                'focus_x'=>$homeCompetitor['image']['focus']['x'],
                'focus_y'=>$homeCompetitor['image']['focus']['y']
            ];
            $newImage = $this->imagesRepository->createImage($image);
            $newHomeCompetitor->setImages($newImage);

        }
        $this->add($newHomeCompetitor);
        $persistenceManager->persistAll();

        return $newHomeCompetitor;
    }

    public function edit($homeCompetitor,$editHomeCompetitors){
        $persistenceManager = $this->objectManager->get(\TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager::class);
        foreach ($editHomeCompetitors as $editHomeCompetitor) {
            $editHomeCompetitor->setName($homeCompetitor['name']);

            if ($homeCompetitor['image'] !== NULL) {
                $image = [
                    'image_url' => $homeCompetitor['image']['path'],
                    'focus_x' => $homeCompetitor['image']['focus']['x'],
                    'focus_y' => $homeCompetitor['image']['focus']['y']
                ];
                $editImage = $this->imagesRepository->edit($editHomeCompetitor->getImages(), $image);
                $editHomeCompetitor->setImages($editImage);

            }
            $this->update($editHomeCompetitor);
        }

        $persistenceManager->persistAll();

        return $editHomeCompetitor;
    }
}