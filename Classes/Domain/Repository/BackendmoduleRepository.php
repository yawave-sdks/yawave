<?php


namespace Yawave\Yawave\Domain\Repository;


use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Database\Query\Restriction\DeletedRestriction;
use TYPO3\CMS\Extbase\Annotation\Inject;

class BackendmoduleRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{

    /**
     * CategoryRepository
     *
     * @var \Yawave\Yawave\Domain\Repository\CategoryRepository
     * @Inject
     */
    protected $categoryRepository = NULL;

    /**
     * TagRepository
     *
     * @var \Yawave\Yawave\Domain\Repository\TagRepository
     * @Inject
     */
    protected $tagRepository = NULL;




    //Need to change query to delete specific items from tables acording to type ex: "Tx_Yawave_Publications"

    public function emptyTables(){


        // Delete mm relation for yawave categories
        $querySysCategoryMM = GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable('sys_category_record_mm')->createQueryBuilder();
        $queryPublicationTagMM = GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable('tx_news_domain_model_news_tag_mm')->createQueryBuilder();
        $querySysCategory = GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable('sys_category')->createQueryBuilder();
        $queryPublicationTags = GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable('tx_news_domain_model_tag')->createQueryBuilder();
        $querySysFileReferenc = GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable('sys_file_reference')->createQueryBuilder();

        foreach($this->categoryRepository->findYawaveCategories() as $categoryId){
            $categoryIds[] = $categoryId->getUid();
        }

        if($categoryIds !== NULL) {
            $querySysCategoryMM
                ->delete('sys_category_record_mm')
                ->where(
                    $querySysCategoryMM->expr()->in('uid_local', $categoryIds)
                )
                ->execute();
        }

        foreach($this->tagRepository->findYawaveTags() as $tagId){
            $tagsIds[] = $tagId->getUid();
        }

        if($tagsIds !== NULL){
            $queryPublicationTagMM
                ->delete('tx_news_domain_model_news_tag_mm')
                ->where(
                    $queryPublicationTagMM->expr()->in('uid_foreign', $tagsIds)
                )
                ->execute();
        }



        // Delete all the categories from yawave
        $querySysCategory
            ->delete('sys_category')
            ->where(
                $querySysCategory->expr()->notLike('yawave_category_id', '0')
            )
            ->execute();

        // Delete all the Tags for Yawave
        $queryPublicationTags
            ->delete('tx_news_domain_model_tag')
            ->where(
                $queryPublicationTags->expr()->notLike('yawave_tag_id', '0')
            )
            ->execute();


        $queryPublicationTags
            ->delete('tx_news_domain_model_tag')
            ->where(
                $queryPublicationTags->expr()->notLike('yawave_tag_id', '0')
            )
            ->execute();

        // Delete File reference only for Yawave records
        $querySysFileReferenc
            ->delete('sys_file_reference')
            ->where(
                $querySysFileReferenc->expr()->eq('tablenames', $querySysFileReferenc->createNamedParameter('tx_yawave_domain_model_images'))
            )
            ->execute();


        // Delete News record only for Yawave
        GeneralUtility::makeInstance(ConnectionPool::class)
            ->getConnectionForTable('tx_news_domain_model_news')
            ->delete(
                'tx_news_domain_model_news', // from
                [ 'type' => 'Tx_Yawave_Publications' ] // where
            );

        // Delete Related records
        GeneralUtility::makeInstance(ConnectionPool::class)
            ->getConnectionForTable('tx_news_domain_model_news_related_mm')
            ->truncate('tx_news_domain_model_news_related_mm');


        GeneralUtility::makeInstance(ConnectionPool::class)
            ->getConnectionForTable('tx_yawave_covers_images_mm')
            ->truncate('tx_yawave_covers_images_mm');
        GeneralUtility::makeInstance(ConnectionPool::class)
            ->getConnectionForTable('tx_yawave_covers_title_image_mm')
            ->truncate('tx_yawave_covers_title_image_mm');
        GeneralUtility::makeInstance(ConnectionPool::class)
            ->getConnectionForTable('tx_yawave_domain_model_covers')
            ->truncate('tx_yawave_domain_model_covers');
        GeneralUtility::makeInstance(ConnectionPool::class)
            ->getConnectionForTable('tx_yawave_domain_model_headers')
            ->truncate('tx_yawave_domain_model_headers');
        GeneralUtility::makeInstance(ConnectionPool::class)
            ->getConnectionForTable('tx_yawave_domain_model_icons')
            ->truncate('tx_yawave_domain_model_icons');
        GeneralUtility::makeInstance(ConnectionPool::class)
            ->getConnectionForTable('tx_yawave_domain_model_images')
            ->truncate('tx_yawave_domain_model_images');
        GeneralUtility::makeInstance(ConnectionPool::class)
            ->getConnectionForTable('tx_yawave_domain_model_metrics')
            ->truncate('tx_yawave_domain_model_metrics');
        GeneralUtility::makeInstance(ConnectionPool::class)
            ->getConnectionForTable('tx_yawave_domain_model_portals')
            ->truncate('tx_yawave_domain_model_portals');
        GeneralUtility::makeInstance(ConnectionPool::class)
            ->getConnectionForTable('tx_yawave_domain_model_references')
            ->truncate('tx_yawave_domain_model_references');
        GeneralUtility::makeInstance(ConnectionPool::class)
            ->getConnectionForTable('tx_yawave_domain_model_relatedcontent')
            ->truncate('tx_yawave_domain_model_relatedcontent');
        GeneralUtility::makeInstance(ConnectionPool::class)
            ->getConnectionForTable('tx_yawave_domain_model_tools')
            ->truncate('tx_yawave_domain_model_tools');
        GeneralUtility::makeInstance(ConnectionPool::class)
            ->getConnectionForTable('tx_yawave_domain_model_update')
            ->truncate('tx_yawave_domain_model_update');
        GeneralUtility::makeInstance(ConnectionPool::class)
            ->getConnectionForTable('tx_yawave_portals_images_mm')
            ->truncate('tx_yawave_portals_images_mm');
        GeneralUtility::makeInstance(ConnectionPool::class)
            ->getConnectionForTable('tx_yawave_portals_publications_mm')
            ->truncate('tx_yawave_portals_publications_mm');
        GeneralUtility::makeInstance(ConnectionPool::class)
            ->getConnectionForTable('tx_yawave_portals_relatedcontent_mm')
            ->truncate('tx_yawave_portals_relatedcontent_mm');




        GeneralUtility::makeInstance(ConnectionPool::class)
            ->getConnectionForTable('tx_yawave_domain_model_awaycompetitor')
            ->truncate('tx_yawave_domain_model_awaycompetitor');
        GeneralUtility::makeInstance(ConnectionPool::class)
            ->getConnectionForTable('tx_yawave_domain_model_awaycompetitorexternal')
            ->truncate('tx_yawave_domain_model_awaycompetitorexternal');
        GeneralUtility::makeInstance(ConnectionPool::class)
            ->getConnectionForTable('tx_yawave_domain_model_competition')
            ->truncate('tx_yawave_domain_model_competition');
        GeneralUtility::makeInstance(ConnectionPool::class)
            ->getConnectionForTable('tx_yawave_domain_model_homecompetitor')
            ->truncate('tx_yawave_domain_model_homecompetitor');
        GeneralUtility::makeInstance(ConnectionPool::class)
            ->getConnectionForTable('tx_yawave_domain_model_homecompetitorexternal')
            ->truncate('tx_yawave_domain_model_homecompetitorexternal');
        GeneralUtility::makeInstance(ConnectionPool::class)
            ->getConnectionForTable('tx_yawave_domain_model_liveblog')
            ->truncate('tx_yawave_domain_model_liveblog');
        GeneralUtility::makeInstance(ConnectionPool::class)
            ->getConnectionForTable('tx_yawave_domain_model_liveblogpost')
            ->truncate('tx_yawave_domain_model_liveblogpost');
        GeneralUtility::makeInstance(ConnectionPool::class)
            ->getConnectionForTable('tx_yawave_domain_model_livtickerpostupdate')
            ->truncate('tx_yawave_domain_model_livtickerpostupdate');
        GeneralUtility::makeInstance(ConnectionPool::class)
            ->getConnectionForTable('tx_yawave_domain_model_livtickerupdate')
            ->truncate('tx_yawave_domain_model_livtickerupdate');
        GeneralUtility::makeInstance(ConnectionPool::class)
            ->getConnectionForTable('tx_yawave_domain_model_players')
            ->truncate('tx_yawave_domain_model_players');
        GeneralUtility::makeInstance(ConnectionPool::class)
            ->getConnectionForTable('tx_yawave_domain_model_sources')
            ->truncate('tx_yawave_domain_model_sources');
        GeneralUtility::makeInstance(ConnectionPool::class)
            ->getConnectionForTable('tx_yawave_liveblogpost_players_mm')
            ->truncate('tx_yawave_liveblogpost_players_mm');
        GeneralUtility::makeInstance(ConnectionPool::class)
            ->getConnectionForTable('tx_yawave_liveblog_sources_mm')
            ->truncate('tx_yawave_liveblog_sources_mm');
        GeneralUtility::makeInstance(ConnectionPool::class)
            ->getConnectionForTable('tx_yawave_domain_model_competitor')
            ->truncate('tx_yawave_domain_model_competitor');
    }

    /**
     * Returns all objects of this repository.
     *
     * @return QueryResultInterface|array
     * @api
     */
    public function findAll()
    {

        $query = $this->createQuery();
        $query->getQuerySettings()
            ->setRespectStoragePage(false);

        return $query->execute();

    }
}
