<?php


namespace Yawave\Yawave\Domain\Repository;


use Yawave\Yawave\Domain\Model\Liveblog;
use TYPO3\CMS\Extbase\Annotation\Inject;
use TYPO3\CMS\Core\Database\Query\QueryBuilder;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Extbase\Persistence\Generic\Storage\Typo3DbQueryParser;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;


class LiveblogRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{

    /**
     * @var array
     */
    protected $defaultOrderings = array(
        'uid' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
    );

    /**
     * ImagesRepository
     *
     * @var \Yawave\Yawave\Domain\Repository\ImagesRepository
     * @Inject
     */
    protected $imagesRepository = NULL;

    /**
     * HomeCompetitorRepository
     *
     * @var \Yawave\Yawave\Domain\Repository\HomeCompetitorRepository
     * @Inject
     */
    protected $homeCompetitorRepository = NULL;

    /**
     * HomeCompetitorRepository
     *
     * @var \Yawave\Yawave\Domain\Repository\AwayCompetitorRepository
     * @Inject
     */
    protected $awayCompetitorRepository = NULL;

    /**
     * TagRepository
     *
     * @var \Yawave\Yawave\Domain\Repository\TagRepository
     * @Inject
     */
    protected $tagRepository = NULL;

    /**
     * CategoryRepository
     *
     * @var \Yawave\Yawave\Domain\Repository\CategoryRepository
     * @Inject
     */
    protected $categoryRepository = NULL;

    /**
     * SourcesRepository
     *
     * @var \Yawave\Yawave\Domain\Repository\SourcesRepository
     * @Inject
     */
    protected $sourcesRepository = NULL;


    public function createLiveblog($liveBlog){
        $persistenceManager = $this->objectManager->get(\TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager::class);
        $newLiveBlog = new Liveblog();
        $newLiveBlog->setLiveblogId($liveBlog['id']);
        $newLiveBlog->setTitle($liveBlog['title']);
        $newLiveBlog->setDescription($liveBlog['description']);

        if($liveBlog['image'] !== NULL) {
            $image = [
                'image_url'=>$liveBlog['image']['path'],
                'focus_x'=>$liveBlog['image']['focus']['x'],
                'focus_y'=>$liveBlog['image']['focus']['y']
            ];
            $newImage = $this->imagesRepository->createImage($image);
            $newLiveBlog->setImages($newImage);
        }

        if(!empty($liveBlog['category_ids'])){

            // Add All categories for news
            $newCategories = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
            $categories = $this->categoryRepository->findCategories($liveBlog['category_ids'],0);
            foreach ($categories as $category){
                $newCategories->attach($category);
            }
            $newLiveBlog->setLiveblogCategoryId($newCategories);

        }

        if(!empty($liveBlog['tag_ids'])){
            $newTags = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
            $tags = $this->tagRepository->findTags($liveBlog['tag_ids']);
            foreach ($tags as $tag) {
                $newTags->attach($tag);
            }
            $newLiveBlog->setYawaveTagId($newTags);
        }

        $newLiveBlog->setType($liveBlog['type']);
        $newLiveBlog->setStatus($liveBlog['status']);
        $newLiveBlog->setLocation($liveBlog['location']);

        $createDate = new \DateTime($liveBlog['start_date']);
        $newLiveBlog->setStartDate($createDate->getTimestamp());

        if(!empty($liveBlog['home_competitor'])){
            $newHomeCompetitor = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
            $homeCompetitors = $this->homeCompetitorRepository->create($liveBlog['home_competitor']);
            $newHomeCompetitor->attach($homeCompetitors);
            $newLiveBlog->setHomeCompetitor($newHomeCompetitor);
        }

        if(!empty($liveBlog['away_competitor'])){
            $newAwayCompetitor = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
            $awayCompetitors = $this->awayCompetitorRepository->create($liveBlog['away_competitor']);
            $newAwayCompetitor->attach($awayCompetitors);
            $newLiveBlog->setAwayCompetitor($newAwayCompetitor);
        }


        if(!empty($liveBlog['sources'])){
            $newSources = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
            $sources = $this->sourcesRepository->create($liveBlog['sources']);
            $newSources->attach($sources);
            $newLiveBlog->setSources($newSources);
            $newLiveBlog->setSportEventId($liveBlog['sources'][0]['sport_event_id']);

        }

        $this->add($newLiveBlog);
        $persistenceManager->persistAll();
        return $newLiveBlog;


    }

    public function updateLiveblog($editLiveBlog,$liveBlog){
        $persistenceManager = $this->objectManager->get(\TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager::class);

        $editLiveBlog->setLiveblogId($liveBlog['id']);
        $editLiveBlog->setTitle($liveBlog['title']);
        $editLiveBlog->setDescription($liveBlog['description']);

        if($liveBlog['image'] !== NULL) {
            $image = [
                'image_url'=>$liveBlog['image']['path'],
                'focus_x'=>$liveBlog['image']['focus']['x'],
                'focus_y'=>$liveBlog['image']['focus']['y']
            ];
            $editImage = $this->imagesRepository->edit($editLiveBlog->getImages(),$image);
            $editLiveBlog->setImages($editImage);
        }

        if(!empty($liveBlog['category_ids'])){

            // Add All categories for news
            $newCategories = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
            $categories = $this->categoryRepository->findCategories($liveBlog['category_ids'],0);
            foreach ($categories as $category){
                $newCategories->attach($category);
            }
            $editLiveBlog->setLiveblogCategoryId($newCategories);
        }

        if(!empty($liveBlog['tag_ids'])) {
            // Find Tags and set it to Publication
            $newTags = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
            $tags = $this->tagRepository->findTags($liveBlog['tag_ids']);
            foreach ($tags as $tag) {
                $newTags->attach($tag);
            }
            $editLiveBlog->setYawaveTagId($newTags);
        }

        $editLiveBlog->setType($liveBlog['type']);
        $editLiveBlog->setStatus($liveBlog['status']);
        $editLiveBlog->setLocation($liveBlog['location']);

        $createDate = new \DateTime($liveBlog['start_date']);
        $editLiveBlog->setStartDate($createDate->getTimestamp());

        if(!empty($liveBlog['home_competitor'])){
            $newHomeCompetitor = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
            $editHomeCompetitor = $editLiveBlog->getHomeCompetitor();
            $homeCompetitors = $this->homeCompetitorRepository->edit($liveBlog['home_competitor'],$editHomeCompetitor);
            $newHomeCompetitor->attach($homeCompetitors);
            $editLiveBlog->setHomeCompetitor($newHomeCompetitor);
        }

        if(!empty($liveBlog['away_competitor'])){
            $newAwayCompetitor = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
            $editAwayCompetitors = $editLiveBlog->getAwayCompetitor();
            $awayCompetitors = $this->awayCompetitorRepository->edit($liveBlog['away_competitor'],$editAwayCompetitors);
            $newAwayCompetitor->attach($awayCompetitors);
            $editLiveBlog->setAwayCompetitor($newAwayCompetitor);
        }

        if(!empty($liveBlog['sources'])){
            $newSources = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
            $editSources = $editLiveBlog->getSources();
            $sources = $this->sourcesRepository->edit($liveBlog['sources'],$editSources);
            $newSources->attach($sources);
            $editLiveBlog->setSources($newSources);

            $editLiveBlog->setSportEventId($liveBlog['sources'][0]['sport_event_id']);

        }
        $this->update($editLiveBlog);
        $persistenceManager->persistAll();
        return $editLiveBlog;
    }

    public function delete($deleteLiveBlog){
        $persistenceManager = $this->objectManager->get(\TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager::class);
        $this->remove($deleteLiveBlog);
        $persistenceManager->persistAll();
    }
    /**
     *
     *
     * @param \Yawave\Yawave\Domain\Model\Liveblog $liveBlog
     * @param \Yawave\Yawave\Domain\Model\Liveblogpost $blogPost
     * @return void
     */
    public function addPostToBlog($liveBlog,$blogPost){
        $persistenceManager = $this->objectManager->get(\TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager::class);
        $liveBlog->addPost($blogPost);
        $this->update($liveBlog);
        $persistenceManager->persistAll();
        return;
    }

    public function findAll(){
        $query = $this->createQuery();
        $query->getQuerySettings()
            ->setRespectStoragePage(false);

        return $query->execute();
    }

    public function findBlogByIdOrCategoryOrSportEventId($blogId=NULL,$categoryTitle=NULL,$sportEventId=NULL){

        $query = $this->createQuery();
        $query->getQuerySettings()
            ->setRespectStoragePage(false);
        $constraints = $query->logicalOr(
            [
                $query->equals('uid', $blogId),
                $query->equals('liveblogCategoryId.title', $categoryTitle),
                $query->equals('sources.sportEventId', $sportEventId),
            ]
        );

        $result = $query->matching($constraints)->execute();
        return $result;

    }




}