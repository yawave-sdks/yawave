<?php


namespace Yawave\Yawave\Domain\Repository;

use Yawave\Yawave\Domain\Model\AwayCompetitorExternal;
use TYPO3\CMS\Extbase\Annotation\Inject;

class AwayCompetitorExternalRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
    /**
     * ImagesRepository
     *
     * @var \Yawave\Yawave\Domain\Repository\ImagesRepository
     * @Inject
     */
    protected $imagesRepository = NULL;

    public function create($awayCompetitorExternal){
        $persistenceManager = $this->objectManager->get(\TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager::class);
        $newaAwayCompetitorExternal = new AwayCompetitorExternal();
        $newaAwayCompetitorExternal->setName($awayCompetitorExternal['name']);

        if($awayCompetitorExternal['image'] !== NULL) {
            $image = [
                'image_url'=>$awayCompetitorExternal['image']['path'],
                'focus_x'=>$awayCompetitorExternal['image']['focus']['x'],
                'focus_y'=>$awayCompetitorExternal['image']['focus']['y']
            ];
            $newImage = $this->imagesRepository->createImage($image);
            $newaAwayCompetitorExternal->setImages($newImage);

        }
        $this->add($newaAwayCompetitorExternal);
        $persistenceManager->persistAll();

        return $newaAwayCompetitorExternal;
    }


    public function edit($awayCompetitorExternal,$editawayCompetitorExternals){
        $editawayCompetitorExternal = $editawayCompetitorExternals->current();
        $persistenceManager = $this->objectManager->get(\TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager::class);

        $editawayCompetitorExternal->setName($awayCompetitorExternal['name']);

        if($awayCompetitorExternal['image'] !== NULL) {
            $image = [
                'image_url'=>$awayCompetitorExternal['image']['path'],
                'focus_x'=>$awayCompetitorExternal['image']['focus']['x'],
                'focus_y'=>$awayCompetitorExternal['image']['focus']['y']
            ];
            $newImage = $this->imagesRepository->edit($awayCompetitorExternal->getImages(),$image);
            $editawayCompetitorExternal->setImages($newImage);

        }
        $this->add($editawayCompetitorExternal);
        $persistenceManager->persistAll();

        return $editawayCompetitorExternal;
    }


}