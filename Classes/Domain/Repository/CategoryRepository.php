<?php

namespace Yawave\Yawave\Domain\Repository;


use Doctrine\DBAL\Connection;
use GeorgRinger\News\Domain\Model\DemandInterface;
use GeorgRinger\News\Service\CategoryService;
use Yawave\Yawave\Domain\Model\Category;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\QueryInterface;
use TYPO3\CMS\Extbase\Annotation\Inject;
use Yawave\Yawave\Service\ErrorMessage;


class CategoryRepository extends \GeorgRinger\News\Domain\Repository\CategoryRepository
{

    /**
     * BackendModuleRepository
     *
     * @var \Yawave\Yawave\Domain\Repository\BackendmoduleRepository
     * @Inject
     */
    protected $backendmoduleRepository = NULL;

    /**
     * @var \TYPO3\CMS\Extbase\Object\ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * @param \TYPO3\CMS\Extbase\Object\ObjectManagerInterface $objectManager
     */
    public function injectObjectManager(\TYPO3\CMS\Extbase\Object\ObjectManagerInterface $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    public function backendConfiguration(){
        return $this->backendmoduleRepository->findAll()->getFirst();
    }



    /**
     * check tags
     *
     * @param array $categories
     * @param int $currentCategoriesPage
     *
     */
    public function checkCategories($categoriesPerLanguage,$currentCategoriesPage=0){

        $configuratedlanguages = $this->backendConfiguration();
        $ordered = array();

        if($configuratedlanguages->getLanguageIds() !== '') {
            foreach ($configuratedlanguages->getLanguageIds() as $key => $languageId) {
                if (array_key_exists($key, $categoriesPerLanguage)) {
                    $ordered[$key] = $categoriesPerLanguage[$key];
                    unset($categoriesPerLanguage[$key]);
                }
            }


            foreach ($ordered as $key => $categories){
                $this->checkLanguge($key,$categories);
            }
        }else {
            $body = "You did't set yawave in static info tables! or you didn't set languages id-s in typoscript ";
            $header = 'Configuration';
            return (new ErrorMessage())->showError($header, $body);
        }

    }



    public function checkLanguge($categoryLanguage,$yawaveCategories){

        $configuratedlanguages = $this->backendConfiguration();
        $persistenceManager = $this->objectManager->get(\TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager::class);

        foreach ($configuratedlanguages->getLanguageIds() as $key => $languageId) {
            if ($key == $categoryLanguage) {
                $languageIds = $languageId;
                $languageShort = $key;
            }
        }


        $existedCategory = $this->findCategoryByLanguage($yawaveCategories, $languageIds);


        if (empty($existedCategory)) {
            $this->create($yawaveCategories, $languageIds, $languageShort);
            $this->setCategories();
        }
        $persistenceManager->persistAll();
    }


    // Create Categories
    public function create($categories,$languageId,$languageShort)
    {

        $persistenceManager = $this->objectManager->get(\TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager::class);

        foreach($categories as $category){
            $newCategory = new Category();
            $newCategory->setTitle($category['name']);
            if(intval($languageId) !== 0){
                $parentCategory = $this->findParentCategory($category['id']);
                if($parentCategory->count() > 0) {
                    $newCategory->setL10nParent($parentCategory[0]->getUid());
                }
            }
            $newCategory->setSysLanguageUid($languageId);
            $newCategory->setSlug($category['slug']);
            $newCategory->setYawaveCategoryId($category['id']);
            $newCategory->setYawaveCategoryParentId($category['parent_id']);
            $this->add($newCategory);
            $persistenceManager->persistAll();
        }

    }

    //Set categories to parent category
    public  function setCategories(){
        $categories = $this->subCategory();
        if($categories !== NULL) {
            $persistenceManager = $this->objectManager->get(\TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager::class);
            foreach ($categories as $key => $category) {
                foreach ($category as $cat) {
                    $cat->setParentcategory($this->findByUid($key));
                    $this->update($cat);
                    $persistenceManager->persistAll();
                }
            }
        }
    }

    // Create sub Category
    public function subCategory(){
        $allCategories = $this->findAll();
        foreach ($allCategories as $category){
            $categoryId = $category->getUid();
            $yawaveCategoryId = $category->getYawaveCategoryId();

            $query = $this->createQuery();
            $query->matching(
                $query->equals('yawave_category_parent_id', $yawaveCategoryId)
            );
            $allResults = $query->execute();

            if(count($allResults) > 0){
                $result[$categoryId] = $allResults;
            }
        }
        return $result;
    }


    // Find Category by Id
    public function findCategories($ids,$languageId){

        $query = $this->createQuery();
        $query->getQuerySettings()->setLanguageUid($languageId);
        $query->matching(
            $query->in('yawave_category_id', $ids)
        );
        return $query->execute();
    }

    // Find Yawave Category
    public function findYawaveCategories(){
        $query = $this->createQuery();
        $query->matching(
            $query->logicalNot(
                $query->equals('yawave_category_id', '0')
            )
        );
        return $query->execute();
    }


    public function findCategoryByLanguage($yawaveId,$languageId){

        $query = $this->createQuery();
        $query->getQuerySettings()
            ->setRespectSysLanguage(false)
            ->setLanguageOverlayMode(false)
            ->setRespectStoragePage(false);

        $constraints = $query->logicalAnd(
            [
                $query->equals('yawave_category_id', $yawaveId),
                $query->equals('sys_language_uid', $languageId),
                $query->equals('deleted', 0),
            ]
        );

        $result = $query->matching($constraints)->execute();
        return $result->getFirst();


    }


    //Find Default Language Category
    public function findParentCategory($categoryId){

        $query = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage(false);
        $query->getQuerySettings()->setRespectSysLanguage(false);
        $query->getQuerySettings()->setIgnoreEnableFields(true);
        $query->matching(
            $query->logicalAnd(
                [
                    $query->equals('yawave_category_id', $categoryId),
                    $query->equals('sys_language_uid', 0),
                ]
            )
        );

        return $query->execute();

    }


}
