<?php


namespace Yawave\Yawave\Domain\Repository;

use Yawave\Yawave\Domain\Model\AwayCompetitor;
use TYPO3\CMS\Extbase\Annotation\Inject;

class AwayCompetitorRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
    /**
     * ImagesRepository
     *
     * @var \Yawave\Yawave\Domain\Repository\ImagesRepository
     * @Inject
     */
    protected $imagesRepository = NULL;

    public function create($awayCompetitor){
        $persistenceManager = $this->objectManager->get(\TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager::class);
        $newaAwayCompetitor = new AwayCompetitor();
        $newaAwayCompetitor->setName($awayCompetitor['name']);

        if($awayCompetitor['image'] !== NULL) {
            $image = [
                'image_url'=>$awayCompetitor['image']['path'],
                'focus_x'=>$awayCompetitor['image']['focus']['x'],
                'focus_y'=>$awayCompetitor['image']['focus']['y']
            ];
            $newImage = $this->imagesRepository->createImage($image);
            $newaAwayCompetitor->setImages($newImage);

        }
        $this->add($newaAwayCompetitor);
        $persistenceManager->persistAll();

        return $newaAwayCompetitor;
    }


    public function edit($awayCompetitor,$editAwayCompetitors){
        $persistenceManager = $this->objectManager->get(\TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager::class);
        foreach ($editAwayCompetitors as $editAwayCompetitor) {
            $editAwayCompetitor->setName($awayCompetitor['name']);
            if ($awayCompetitor['image'] !== NULL) {
                $image = [
                    'image_url' => $awayCompetitor['image']['path'],
                    'focus_x' => $awayCompetitor['image']['focus']['x'],
                    'focus_y' => $awayCompetitor['image']['focus']['y']
                ];
                $editImage = $this->imagesRepository->edit($editAwayCompetitor->getImages(), $image);
                $editAwayCompetitor->setImages($editImage);

            }
            $this->update($editAwayCompetitor);
            $persistenceManager->persistAll();
        }
        return $editAwayCompetitor;
    }

}