<?php


namespace Yawave\Yawave\Domain\Repository;


use Yawave\Yawave\Domain\Model\HomeCompetitorExternal;
use TYPO3\CMS\Extbase\Annotation\Inject;

class HomeCompetitorExternalRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
    /**
     * ImagesRepository
     *
     * @var \Yawave\Yawave\Domain\Repository\ImagesRepository
     * @Inject
     */
    protected $imagesRepository = NULL;

    public function create($homeCompetitorExternal){
        $persistenceManager = $this->objectManager->get(\TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager::class);
        $newHomeCompetitorExternal = new HomeCompetitorExternal();
        $newHomeCompetitorExternal->setName($homeCompetitorExternal['name']);

        if($homeCompetitorExternal['image'] !== NULL) {
            $image = [
                'image_url'=>$homeCompetitorExternal['image']['path'],
                'focus_x'=>$homeCompetitorExternal['image']['focus']['x'],
                'focus_y'=>$homeCompetitorExternal['image']['focus']['y']
            ];
            $newImage = $this->imagesRepository->createImage($image);
            $newHomeCompetitorExternal->setImages($newImage);

        }
        $this->add($newHomeCompetitorExternal);
        $persistenceManager->persistAll();

        return $newHomeCompetitorExternal;
    }

    public function edit($homeCompetitorExternal,$edithomeCompetitorExternals){
        $edithomeCompetitorExternal = $edithomeCompetitorExternals->current();
        $persistenceManager = $this->objectManager->get(\TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager::class);

        $edithomeCompetitorExternal->setName($homeCompetitorExternal['name']);

        if($homeCompetitorExternal['image'] !== NULL) {
            $image = [
                'image_url'=>$homeCompetitorExternal['image']['path'],
                'focus_x'=>$homeCompetitorExternal['image']['focus']['x'],
                'focus_y'=>$homeCompetitorExternal['image']['focus']['y']
            ];
            $newImage = $this->imagesRepository->edit($edithomeCompetitorExternal->getImages(),$image);
            $edithomeCompetitorExternal->setImages($newImage);

        }
        $this->add($edithomeCompetitorExternal);
        $persistenceManager->persistAll();

        return $edithomeCompetitorExternal;
    }
}