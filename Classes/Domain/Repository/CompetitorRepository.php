<?php


namespace Yawave\Yawave\Domain\Repository;

use Yawave\Yawave\Domain\Model\Competitor;

class CompetitorRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
    public function create($competitor){
        $persistenceManager = $this->objectManager->get(\TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager::class);
        $newCompetitor = new Competitor();
        if($competitor['external_id'] !== NULL) {
            $newCompetitor->setExternalId($competitor['external_id']);
        }
        if($competitor['name'] !== NULL) {
            $newCompetitor->setName($competitor['name']);
        }

        $this->add($newCompetitor);
        $persistenceManager->persistAll();
        return $newCompetitor;
    }

    public function edit($editCompetitor,$competitor){
        $persistenceManager = $this->objectManager->get(\TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager::class);

        if($competitor['external_id'] !== NULL) {
            $editCompetitor->setExternalId($competitor['external_id']);
        }
        if($competitor['name'] !== NULL) {
            $editCompetitor->setName($competitor['name']);
        }

        $this->update($editCompetitor);
        $persistenceManager->persistAll();
        return $editCompetitor;
    }
}