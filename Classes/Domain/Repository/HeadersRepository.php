<?php


namespace Yawave\Yawave\Domain\Repository;

use Yawave\Yawave\Domain\Model\Headers;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\QueryInterface;
use TYPO3\CMS\Extbase\Annotation\Inject;

/***
 *
 * This file is part of the "Yawave" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2020 Epic Software Solutions <info@epicss.io>, Epic Software Solutions
 *
 ***/
/**
 * The repository for Headers
 */

class HeadersRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
    /**
     * ImagesRepository
     *
     * @var \Yawave\Yawave\Domain\Repository\ImagesRepository
     * @Inject
     */
    protected $imagesRepository = NULL;

    /**
     * PublicationRepository
     *
     * @var \Yawave\Yawave\Domain\Repository\PublicationsRepository
     * @Inject
     */
    protected $publicationsRepository = NULL;


    /**
     * @var \TYPO3\CMS\Extbase\Object\ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * @param \TYPO3\CMS\Extbase\Object\ObjectManagerInterface $objectManager
     */
    public function injectObjectManager(\TYPO3\CMS\Extbase\Object\ObjectManagerInterface $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    // Create Header
    public function createHeader($header,$publicationId,$languageId){
        $persistenceManager = $this->objectManager->get(\TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager::class);
        $newImage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $newHeader = new Headers();

        $newHeader->setTitle($header['title']);
        $newHeader->setDescription($header['description']);
        $newHeader->setSysLanguageUid((int)$languageId);
        $newHeader->_setProperty('_languageUid', $languageId);
        if(intval($languageId) !== 0){

            $publication = $this->publicationsRepository->findByYawavePublicationId($publicationId)->getFirst();
            if(isset($publication)) {
                $parentHeader = $publication->getHeader();

                if(isset($parentHeader)) {
                    $newHeader->setL10nParent((int)$parentHeader->getUid());
                }
            }
        }

        if(!empty($header['image'])){

            $image = [
                'image_url'=>$header['image']['path'],
                'focus_x'=>$header['image']['focus']['x'],
                'focus_y'=>$header['image']['focus']['y']
            ];
            $createImage = $this->imagesRepository->createImage($image);
            $newImage->attach($createImage);
            $newHeader->setImage($newImage);
        }
        $this->add($newHeader);
        $persistenceManager->persistAll();
        return $newHeader;
    }

    /**
     * @param \Yawave\Yawave\Domain\Model\Headers $header
     * @param array $updateHeader
     */
    public function editHeader(\Yawave\Yawave\Domain\Model\Headers $header,$updateHeader){
        $persistenceManager = $this->objectManager->get(\TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager::class);
        $newImage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $header->setTitle($updateHeader['title']);
        $header->setDescription($updateHeader['description']);

        if(!empty($updateHeader['image'])){
            $image = [
                'image_url'=>$updateHeader['image']['path'],
                'focus_x'=>$updateHeader['image']['focus']['x'],
                'focus_y'=>$updateHeader['image']['focus']['y']
            ];
            $editImage = $this->imagesRepository->edit($header->getImage()[0],$image);
            $newImage->attach($editImage);
            $header->setImage($newImage);
        }

        $this->update($header);
        $persistenceManager->persistAll();
        return $header;
    }

    public function findHeaderByUidAndLanguageId($header,$languageIds){
        $query = $this->createQuery();
        $query->getQuerySettings()
            ->setRespectSysLanguage(false)
            ->setLanguageOverlayMode(false)
            ->setLanguageMode('ignore')
            ->setIgnoreEnableFields(TRUE)
            ->setRespectStoragePage(false);

        $constraints = $query->logicalAnd(
            [
                $query->equals('l10nParent', $header->getUid()),
                $query->equals('sys_language_uid', $languageIds),
                $query->logicalOr([
                    $query->equals('hidden', 0),
                    $query->equals('hidden', 1),
                ]),
                $query->equals('deleted', 0),
            ]
        );
        $result = $query->matching($constraints)->execute();
        return $result->getFirst();
    }

}