<?php


namespace Yawave\Yawave\Domain\Repository;


use Yawave\Yawave\Domain\Model\Livtickerpostupdate;

class LivtickerpostupdateRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
    /**
     * action update
     *
     * @param Livtickerpostupdate $livtickerpostupdate
     * @return void
     */
    public function removeUpdate($livtickerpostupdate){
        $persistenceManager = $this->objectManager->get(\TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager::class);
        $this->remove($livtickerpostupdate);
        $persistenceManager->persistAll();
    }
}