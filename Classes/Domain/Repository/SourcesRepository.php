<?php


namespace Yawave\Yawave\Domain\Repository;


use Yawave\Yawave\Domain\Model\Sources;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Annotation\Inject;

class SourcesRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{

    /**
     * CoversRepository
     *
     * @var \Yawave\Yawave\Domain\Repository\CompetitionRepository
     * @Inject
     */
    protected $competitionRepository = NULL;

    /**
     * CoversRepository
     *
     * @var \Yawave\Yawave\Domain\Repository\HomeCompetitorExternalRepository
     * @Inject
     */
    protected $homeCompetitorExternalRepository = NULL;

    /**
     * CoversRepository
     *
     * @var \Yawave\Yawave\Domain\Repository\AwayCompetitorExternalRepository
     * @Inject
     */
    protected $awayCompetitorExternalRepository = NULL;

    public function create($sources){

        $persistenceManager = $this->objectManager->get(\TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager::class);
        $newSources = new Sources();

        foreach ($sources as $source){
            if($source['type'] !== NULL) {
                $newSources->setType($source['type']);
            }
            if($source['sport_event_id'] !== NULL) {
                $newSources->setSportEventId($source['sport_event_id']);
            }
            if($source['competition'] !== NULL) {

                $newCompetition = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
                $competition = $this->competitionRepository->create($source['competition']);
                $newCompetition->attach($competition);
                $newSources->setCompetition($newCompetition);
            }


            if($source['home_competitor'] !== NULL) {
                $newHomeCompetitor = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
                $homeCompetitorExternal = $this->homeCompetitorExternalRepository->create($source['home_competitor']);
                $newHomeCompetitor->attach($homeCompetitorExternal);
                $newSources->setHomeCompetitorExternal($newHomeCompetitor);
            }
            if($source['away_competitor'] !== NULL) {

                $newAwayCompetitor = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
                $awayCompetitorExternal = $this->awayCompetitorExternalRepository->create($source['away_competitor']);
                $newAwayCompetitor->attach($awayCompetitorExternal);
                $newSources->setAwayCompetitorExternal($newAwayCompetitor);

            }
        }
        $this->add($newSources);

        $persistenceManager->persistAll();
        return $newSources;
    }

    public function edit($sources,$editSources){

        $persistenceManager = $this->objectManager->get(\TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager::class);
        $editSource = $editSources->current();
        foreach ($sources as $source){
            if($source['type'] !== NULL) {
                $editSource->setType($source['type']);
            }
            if($source['sport_event_id'] !== NULL) {
                $editSource->setSportEventId($source['sport_event_id']);
            }
            if($source['competition'] !== NULL) {
                $newCompetition = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
                $editCompetition = $editSource->getCompetition();
                $competition = $this->competitionRepository->edit($source['competition'],$editCompetition);
                $newCompetition->attach($competition);
                $editSource->setCompetition($newCompetition);
            }

            if($source['home_competitor'] !== NULL) {
                $newHomeCompetitor = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
                $edithomeCompetitorExternal = $editSource->getHomeCompetitorExternal();
                $homeCompetitorExternal = $this->homeCompetitorExternalRepository->edit($source['home_competitor'],$edithomeCompetitorExternal);
                $newHomeCompetitor->attach($homeCompetitorExternal);
                $editSource->setHomeCompetitorExternal($newHomeCompetitor);
            }


            if($source['away_competitor'] !== NULL) {
                $newAwayCompetitor = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
                $editawayCompetitorExternal = $editSource->getAwayCompetitorExternal();
                $awayCompetitorExternal = $this->awayCompetitorExternalRepository->edit($source['away_competitor'],$editawayCompetitorExternal);
                $newAwayCompetitor->attach($awayCompetitorExternal);
                $editSource->setAwayCompetitorExternal($newAwayCompetitor);
            }
        }
        $this->update($editSource);

        $persistenceManager->persistAll();
        return $editSource;
    }
}