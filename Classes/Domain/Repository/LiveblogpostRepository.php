<?php


namespace Yawave\Yawave\Domain\Repository;

use Yawave\Yawave\Domain\Model\Liveblogpost;
use TYPO3\CMS\Extbase\Annotation\Inject;
use TYPO3\CMS\Core\Database\Query\QueryBuilder;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Extbase\Persistence\Generic\Storage\Typo3DbQueryParser;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

class LiveblogpostRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
    /**
     * @var array
     */
    protected $defaultOrderings = ['sorting' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING];

    /**
     * PublicationsRepository
     *
     * @var \Yawave\Yawave\Domain\Repository\PublicationsRepository
     * @Inject
     */
    protected $publicationsRepository = NULL;

    /**
     * CompetitorRepository
     *
     * @var \Yawave\Yawave\Domain\Repository\CompetitorRepository
     * @Inject
     */
    protected $competitorRepository = NULL;

    /**
     * PlayersRepository
     *
     * @var \Yawave\Yawave\Domain\Repository\PlayersRepository
     * @Inject
     */
    protected $playersRepository = NULL;

    public function createLiveblogpost($liveBlogPost,$status){
        $persistenceManager = $this->objectManager->get(\TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager::class);
        $newLiveBlogPost = new Liveblogpost();
        $newLiveBlogPost->setPostId($liveBlogPost['id']);
        $newLiveBlogPost->setSource($liveBlogPost['source']);
        $newLiveBlogPost->setStatus((int)$status);

        if($liveBlogPost['period'] !== NULL) {
            $newLiveBlogPost->setPeriods($liveBlogPost['period']);
        }

        if($liveBlogPost['minute'] !== NULL) {
            $newLiveBlogPost->setMinute($liveBlogPost['minute']);
        }

        if($liveBlogPost['stoppage_time'] !== NULL) {
            $newLiveBlogPost->setStoppageTime($liveBlogPost['stoppage_time']);
        }

        $newLiveBlogPost->setTitle($liveBlogPost['title']);

        if($liveBlogPost['text'] !== NULL) {
            $newLiveBlogPost->setText($liveBlogPost['text']);
        }

        if($liveBlogPost['url'] !== NULL) {
            $newLiveBlogPost->setUrl($liveBlogPost['url']);
        }

        if($liveBlogPost['publication_id'] !== NULL) {
            $publication = $this->publicationsRepository->findPublicationForLivetickerBlogPost($liveBlogPost['publication_id'])->getFirst();
            $newLiveBlogPost->setPublicationId($publication);
        }

        if($liveBlogPost['embed_code'] !== NULL) {
            $newLiveBlogPost->setEmbedCode($liveBlogPost['embed_code']);
        }

        if($liveBlogPost['pinned'] !== NULL) {
            $newLiveBlogPost->setPinned($liveBlogPost['pinned']);
        }

        if($liveBlogPost['creation_date'] !== NULL) {
            $creationDate = new \DateTime($liveBlogPost['creation_date']);
            $newLiveBlogPost->setCreationDate($creationDate);
        }

        if($liveBlogPost['action_id'] !== NULL) {
            $newLiveBlogPost->setActionId($liveBlogPost['action_id']);
        }

        if($liveBlogPost['person_id'] !== NULL) {
            $newLiveBlogPost->setPersonId($liveBlogPost['person_id']);
        }

        if($liveBlogPost['action'] !== NULL) {
            $newLiveBlogPost->setAction($liveBlogPost['action']);
        }

        if($liveBlogPost['person'] !== NULL) {
            $newLiveBlogPost->setPerson($liveBlogPost['person']);
        }

        $newLiveBlogPost->setExternalId($liveBlogPost['external_id']);

        $newLiveBlogPost->setType($liveBlogPost['type']);

        if($liveBlogPost['external_id'] !== NULL) {
            $newLiveBlogPost->setExternalId($liveBlogPost['external_id']);
        }

        if($liveBlogPost['stoppage_time'] !== NULL) {
            $newLiveBlogPost->setStoppageTime($liveBlogPost['stoppage_time']);
        }

        if($liveBlogPost['match_clock'] !== NULL) {
            $newLiveBlogPost->setMatchClock($liveBlogPost['match_clock']);
        }

        if($liveBlogPost['competitor'] !== NULL) {
            $createCompetitor = $this->competitorRepository->create($liveBlogPost['competitor']);
            $newLiveBlogPost->setCompetitor($createCompetitor);
        }

        if($liveBlogPost['players'] !== NULL) {
            $createPlayers = $this->playersRepository->create($liveBlogPost['players']);
            $newLiveBlogPost->setPlayers($createPlayers);
        }

        if($liveBlogPost['home_score'] !== NULL) {
            $newLiveBlogPost->setHomeScore($liveBlogPost['home_score']);
        }

        if($liveBlogPost['away_score'] !== NULL) {
            $newLiveBlogPost->setAwayScore($liveBlogPost['away_score']);
        }

        if($liveBlogPost['injury_time'] !== NULL) {
            $newLiveBlogPost->setInjuryTime($liveBlogPost['injury_time']);
        }
        if($liveBlogPost['timeline_timestamp'] !== NULL) {
            $timelineTimestamp = new \DateTime($liveBlogPost['timeline_timestamp']);
            $newLiveBlogPost->setTimelineTimestamp($timelineTimestamp);
        }
        $this->add($newLiveBlogPost);
        $persistenceManager->persistAll();
        return $newLiveBlogPost;

    }

    public function updateLiveblogpost($editLiveBlogPost,$liveBlogPost,$status){
        $persistenceManager = $this->objectManager->get(\TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager::class);

        $editLiveBlogPost->setPostId($liveBlogPost['id']);

        $editLiveBlogPost->setSource($liveBlogPost['source']);
        $editLiveBlogPost->setStatus((int)$status);
        if($liveBlogPost['period'] !== NULL) {
            $editLiveBlogPost->setPeriods($liveBlogPost['period']);
        }

        if($liveBlogPost['minute'] !== NULL) {
            $editLiveBlogPost->setMinute($liveBlogPost['minute']);
        }

        if($liveBlogPost['stoppage_time'] !== NULL) {
            $editLiveBlogPost->setStoppageTime($liveBlogPost['stoppage_time']);
        }

        $editLiveBlogPost->setTitle($liveBlogPost['title']);

        if($liveBlogPost['text'] !== NULL) {
            $editLiveBlogPost->setText($liveBlogPost['text']);
        }

        if($liveBlogPost['url'] !== NULL) {
            $editLiveBlogPost->setUrl($liveBlogPost['url']);
        }

        if($liveBlogPost['publication_id'] !== NULL) {
            $publication = $this->publicationsRepository->findPublicationForLivetickerBlogPost($liveBlogPost['publication_id'])->getFirst();
            $editLiveBlogPost->setPublicationId($publication);
        }

        if($liveBlogPost['embed_code'] !== NULL) {
            $editLiveBlogPost->setEmbedCode($liveBlogPost['embed_code']);
        }

        if($liveBlogPost['pinned'] !== NULL) {
            $editLiveBlogPost->setPinned($liveBlogPost['pinned']);
        }

        if($liveBlogPost['creation_date'] !== NULL) {
            $creationDate = new \DateTime($liveBlogPost['creation_date']);
            $editLiveBlogPost->setCreationDate($creationDate);
        }

        if($liveBlogPost['action_id'] !== NULL) {
            $editLiveBlogPost->setActionId($liveBlogPost['action_id']);
        }

        if($liveBlogPost['person_id'] !== NULL) {
            $editLiveBlogPost->setPersonId($liveBlogPost['person_id']);
        }

        if($liveBlogPost['action'] !== NULL) {
            $editLiveBlogPost->setAction($liveBlogPost['action']);
        }

        if($liveBlogPost['person'] !== NULL) {
            $editLiveBlogPost->setPerson($liveBlogPost['person']);
        }

        $editLiveBlogPost->setExternalId($liveBlogPost['external_id']);

        if($liveBlogPost['type'] !== NULL) {
            $editLiveBlogPost->setType($liveBlogPost['type']);
        }else{
            $editLiveBlogPost->setType(0);
        }

        if($liveBlogPost['external_id'] !== NULL) {
            $editLiveBlogPost->setExternalId($liveBlogPost['external_id']);
        }else{
            $editLiveBlogPost->setExternalId(0);
        }

        if($liveBlogPost['stoppage_time'] !== NULL) {
            $editLiveBlogPost->setStoppageTime($liveBlogPost['stoppage_time']);
        }

        if($liveBlogPost['match_clock'] !== NULL) {
            $editLiveBlogPost->setMatchClock($liveBlogPost['match_clock']);
        }

        if($liveBlogPost['competitor'] !== NULL) {
            $editCompetitor = $this->competitorRepository->edit($editLiveBlogPost->getCompetitor(),$liveBlogPost['competitor']);
            $editLiveBlogPost->setCompetitor($editCompetitor);
        }

        if($liveBlogPost['players'] !== NULL) {
            $editPlayers = $this->playersRepository->edit($editLiveBlogPost->getPlayers(),$liveBlogPost['players']);
            $editLiveBlogPost->setPlayers($editPlayers);
        }

        if($liveBlogPost['home_score'] !== NULL) {
            $editLiveBlogPost->setHomeScore($liveBlogPost['home_score']);
        }

        if($liveBlogPost['away_score'] !== NULL) {
            $editLiveBlogPost->setAwayScore($liveBlogPost['away_score']);
        }

        if($liveBlogPost['injury_time'] !== NULL) {
            $editLiveBlogPost->setInjuryTime($liveBlogPost['injury_time']);
        }
        if($liveBlogPost['timeline_timestamp'] !== NULL) {
            $timelineTimestamp = new \DateTime($liveBlogPost['timeline_timestamp']);
            $editLiveBlogPost->setTimelineTimestamp($timelineTimestamp);
        }
        $this->update($editLiveBlogPost);
        $persistenceManager->persistAll();
        return $editLiveBlogPost;
    }


    public function delete($deleteLiveBlogPost){
        $persistenceManager = $this->objectManager->get(\TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager::class);
        $status = 3;
        $deleteLiveBlogPostUpdateStatus = $this->updateDeleteStatus($deleteLiveBlogPost,$status);
        $this->remove($deleteLiveBlogPostUpdateStatus);
        $persistenceManager->persistAll();
    }

    public function updateDeleteStatus($editLiveBlogPost,$status){
        $persistenceManager = $this->objectManager->get(\TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager::class);
        $editLiveBlogPost->setStatus((int)$status);
        $this->update($editLiveBlogPost);
        $persistenceManager->persistAll();
        return $editLiveBlogPost;

    }


    public function findPostsByBlogIdandTime($blogId,$updateFrome=0){
        $query = $this->createQuery();
        $query->getQuerySettings()
            ->setIncludeDeleted(TRUE)
            ->setIgnoreEnableFields(TRUE)
            ->setRespectStoragePage(false);
        $constraints = $query->logicalAnd(
            [
                $query->equals('blog', $blogId),
                $query->greaterThanOrEqual('tstamp', $updateFrome),
            ]
        );
        $query->setOrderings(
            [
                'timeline_timestamp' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_DESCENDING,
                'tstamp' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_DESCENDING,
            ]
        );

        $result = $query->matching($constraints)->execute();
        return $result;

    }
}