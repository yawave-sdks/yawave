<?php


namespace Yawave\Yawave\Domain\Repository;

use Yawave\Yawave\Domain\Model\Livtickerupdate;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\QueryInterface;
use TYPO3\CMS\Extbase\Annotation\Inject;

class LivtickerupdateRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{

    /**
     * action update
     *
     * @param Livtickerupdate $livtickerupdate
     * @return void
     */
    public function removeUpdate($livtickerupdate){
        $persistenceManager = $this->objectManager->get(\TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager::class);
        $this->remove($livtickerupdate);
        $persistenceManager->persistAll();
    }

    public function findAll(){
        $query = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage(false);
        $query->getQuerySettings()->setIgnoreEnableFields(true);

        return $query->execute();
    }

}