<?php


namespace Yawave\Yawave\Domain\Repository;

use Yawave\Yawave\Domain\Model\Players;

class PlayersRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
    public function create($players){
        $persistenceManager = $this->objectManager->get(\TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager::class);
        $newPlayers = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        foreach ($players as $player){
            $newPlayer = new Players();

            if($player['external_id'] !== NULL) {
                $newPlayer->setExternalId($player['external_id']);
            }

            if($player['name'] !== NULL) {
                $newPlayer->setName($player['name']);
            }

            if($player['type'] !== NULL) {
                $newPlayer->setType($player['type']);
            }

            $this->add($newPlayer);
            $persistenceManager->persistAll();
            $newPlayers->attach($newPlayer);
        }

        return $newPlayers;
    }

    public function edit($editPlayers,$players){
        $persistenceManager = $this->objectManager->get(\TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager::class);
        $newPlayers = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        foreach ($players as $player){
            foreach($editPlayers as $editPlayer){
                if($player['external_id'] == $editPlayer->getExternalId()){

                    if($player['name'] !== NULL) {
                        $editPlayer->setName($player['name']);
                    }

                    if($player['type'] !== NULL) {
                        $editPlayer->setType($player['type']);
                    }

                    $this->update($editPlayer);
                    $persistenceManager->persistAll();
                    $newPlayers->attach($editPlayer);
                }
            }

        }

        return $newPlayers;
    }
}