<?php

namespace Yawave\Yawave\Domain\Repository;

use Yawave\Yawave\Domain\Model\Competition;

class CompetitionRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
    public function create($competition)
    {
        $persistenceManager = $this->objectManager->get(\TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager::class);
        $newCompetition = new Competition();

        if ($competition['external_id'] !== NULL) {
            $newCompetition->setExternalId($competition['external_id']);
        }
        if ($competition['name'] !== NULL) {
            $newCompetition->setName($competition['name']);
        }
        $this->add($newCompetition);
        $persistenceManager->persistAll();

        return $newCompetition;
    }

    public function edit($competition,$editCompetitions)
    {
        $persistenceManager = $this->objectManager->get(\TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager::class);
        foreach ($editCompetitions as $editCompetition) {
            if ($competition['external_id'] !== NULL) {
                $editCompetition->setExternalId($competition['external_id']);
            }
            if ($competition['name'] !== NULL) {
                $editCompetition->setName($competition['name']);
            }
            $this->update($editCompetition);
        }
        $persistenceManager->persistAll();

        return $editCompetition;
    }
}