<?php


namespace Yawave\Yawave\Controller;


/***
 *
 * This file is part of the "Yawave" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2020 Epic Software Solutions <info@epicss.io>, Epic Software Solutions
 *
 ***/
/**
 * UpdateController
 */

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\QueryInterface;
use TYPO3\CMS\Extbase\Annotation\Inject;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager;


class UpdateController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * @var \Yawave\Yawave\Domain\Repository\UpdateRepository
     * @Inject
     */
    protected $updateRepository = NULL;

    /**
     * BackendModuleRepository
     *
     * @var \Yawave\Yawave\Domain\Repository\BackendmoduleRepository
     * @Inject
     */
    protected $backendmoduleRepository = NULL;

    /**
     * @var \TYPO3\CMS\Extbase\Object\ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * @param \TYPO3\CMS\Extbase\Object\ObjectManagerInterface $objectManager
     */
    public function injectObjectManager(\TYPO3\CMS\Extbase\Object\ObjectManagerInterface $objectManager)
    {
        $this->objectManager = $objectManager;
    }


    public function initializeAction()
    {
        $settings = $this->backendmoduleRepository->findAll()->getFirst();
        $this->objectManager = GeneralUtility::makeInstance(ObjectManager::class);
        $this->configurationManager = $this->objectManager->get(
            ConfigurationManagerInterface::class
        );
        $curentPageid = (int)GeneralUtility::_GET('id');

        if(!is_null($settings)) {
            $this->configurationManager->setConfiguration(
                [
                    'persistence' => [
                        'storagePid' => $settings->getStoragePid()
                    ],
                ]
            );
        }else{
            $this->configurationManager->setConfiguration(
                [
                    'persistence' => [
                        'storagePid' => $curentPageid
                    ],
                ]
            );
        }

        $this->persistenceManager = $this->objectManager->get(PersistenceManager::class);
    }


    /**
     * action create
     *
     * @param \Yawave\Yawave\Domain\Model\Update $newUpdate
     * @return void
     */
    public function createAction(\Yawave\Yawave\Domain\Model\Update $newUpdate)
    {
        $this->initializeAction();
        $persistenceManager = $this->objectManager->get(\TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager::class);
        $this->updateRepository->add($newUpdate);
        $persistenceManager->persistAll();

        return $newUpdate;
    }

    /**
     * action edit
     *
     * @param \Yawave\Yawave\Domain\Model\Update $update
     * @TYPO3\CMS\Extbase\Annotation\IgnoreValidation $update
     * @return void
     */
    public function editAction(\Yawave\Yawave\Domain\Model\Update $update)
    {
        $this->view->assign('update', $update);
    }

    /**
     * action update
     *
     * @param \Yawave\Yawave\Domain\Model\Update $update
     * @return void
     */
    public function updateAction(\Yawave\Yawave\Domain\Model\Update $update)
    {
        $this->addFlashMessage('The object was updated. Please be aware that this action is publicly accessible unless you implement an access check. See https://docs.typo3.org/typo3cms/extensions/extension_builder/User/Index.html', '', \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING);
        $this->updateRepository->update($update);
        $this->redirect('list');
    }

}