<?php


namespace Yawave\Yawave\Controller;


use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Annotation\Inject;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager;
use Yawave\Yawave\Service\Connection;
use Yawave\Yawave\Service\YawaveRemapLagnuage;
use Yawave\Yawave\Domain\Model\Livtickerupdate;
use Yawave\Yawave\Domain\Model\Livtickerpostupdate;

class LiveTickerController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * BackendModuleRepository
     *
     * @var \Yawave\Yawave\Domain\Repository\BackendmoduleRepository
     * @Inject
     */
    protected $backendmoduleRepository = NULL;

    /**
     * BackendModuleRepository
     *
     * @var \Yawave\Yawave\Domain\Repository\LivtickerupdateRepository
     * @Inject
     */
    protected $livtickerupdateRepository = NULL;

    /**
     * BackendModuleRepository
     *
     * @var \Yawave\Yawave\Domain\Repository\LiveblogRepository
     * @Inject
     */
    protected $liveblogRepository = NULL;

    /**
     * InportLivTickerRecords
     *
     * @var \Yawave\Yawave\Task\InportLivTickerRecords
     * @Inject
     */
    protected $inportLivTickerRecords = NULL;

    /**
     * LivtickerpostupdateRepository
     *
     * @var \Yawave\Yawave\Domain\Repository\LivtickerpostupdateRepository
     * @Inject
     */
    protected $livtickerpostupdateRepository = NULL;

    /**
     * LiveblogpostRepository
     *
     * @var \Yawave\Yawave\Domain\Repository\LiveblogpostRepository
     * @Inject
     */
    protected $liveblogpostRepository = NULL;

    public function initializeAction()
    {

        $settings = $this->backendmoduleRepository->findAll()->getFirst();
        $this->objectManager = GeneralUtility::makeInstance(ObjectManager::class);
        $this->configurationManager = $this->objectManager->get(
            ConfigurationManagerInterface::class
        );
        $curentPageid = (int)GeneralUtility::_GET('id');

        if(!is_null($settings)) {
            $this->configurationManager->setConfiguration(
                [
                    'persistence' => [
                        'storagePid' => $settings->getStoragePid()
                    ],
                ]
            );
        }else{
            $this->configurationManager->setConfiguration(
                [
                    'persistence' => [
                        'storagePid' => $curentPageid
                    ],
                ]
            );
        }

        $this->persistenceManager = $this->objectManager->get(PersistenceManager::class);
    }

    /**
     * Yawave configuration get from extension configuration in InstallTool
     */
    public function yawaveConfiguration(){

        $beConfiguration = $this->backendmoduleRepository->findAll()->getFirst();

        if(!is_null($beConfiguration)) {

            $settings = [
                'client_id' => $clientId = $beConfiguration->getClientId(),
                'secret' => $beConfiguration->getSecret(),
                'token' => $beConfiguration->getHooktoken(),
                'domain' => $beConfiguration->getApiDomain(),
                'storageId' => $beConfiguration->getStoragePid()
            ];
            return $settings;
        }

    }

    /**
     * Yawave connection and get applicationId and client
     *
     * @param array $settings
     */
    public function connectToYawave(array $settings){

        if(!empty($settings['client_id']) and !empty($settings['secret']) and !empty($settings['token']) and !empty($settings['domain'])) {

            $yawave = (new Connection())->apiConnection($settings['client_id'],$settings['secret'],$settings['domain']);

            return $yawave;
        }

        $notSuccessfulMessage= [
            'body'=>'The configuration is not set properly. Check Client Id and Secret',
            'header'=>'Configuration',
            'error' => 400
        ];
        return $notSuccessfulMessage;

    }




    public function pushNotificationAction()
    {

        $bodyData = json_decode(file_get_contents('php://input'), true);
        $allheadersData = getallheaders();


        if($bodyData['event_type'] == 'LIVE_BLOG_CREATED' || $bodyData['event_type'] == 'LIVE_BLOG_UPDATED' || $bodyData['event_type'] == 'LIVE_BLOG_DELETED') {
            $data = [
                'key' => $allheadersData['Authorization'],
                'event_type' => $bodyData['event_type'],
                'application_uuid' => $bodyData['application_uuid'],
                'liveblog_uuid' => $bodyData['liveblog_uuid']
            ];

            $livtickerupdate = new Livtickerupdate();
            $livtickerupdate->setApplicationUuid($data['application_uuid']);
            $livtickerupdate->setEventType($data['event_type']);
            $livtickerupdate->setLiveblogUuid($data['liveblog_uuid']);
            $this->createAction($livtickerupdate);
            $this->inportLivTickerRecords->execute();
        }
        if($bodyData['event_type'] == 'LIVE_BLOG_POST_CREATED' || $bodyData['event_type'] == 'LIVE_BLOG_POST_UPDATED' || $bodyData['event_type'] == 'LIVE_BLOG_POST_DELETED') {
            $data = [
                'key' => $allheadersData['Authorization'],
                'event_type' => $bodyData['event_type'],
                'application_uuid' => $bodyData['application_uuid'],
                'liveblog_uuid' => $bodyData['liveblog_uuid'],
                'liveblog_post_uuid' => $bodyData['liveblog_post_uuid']
            ];
            $livtickerpostupdate = new Livtickerpostupdate();
            $livtickerpostupdate->setApplicationUuid($data['application_uuid']);
            $livtickerpostupdate->setEventType($data['event_type']);
            $livtickerpostupdate->setLiveblogUuid($data['liveblog_uuid']);
            $livtickerpostupdate->setLiveblogPostUuid($data['liveblog_post_uuid']);

            $this->createLiveBlogPostUpdate($livtickerpostupdate);

            $this->inportLivTickerRecords->execute();
        }

        $message = [
            "message" => "Successfully add livTicker Update "
        ];

        return json_encode($message);

    }


    /**
     * action create
     *
     * @param \Yawave\Yawave\Domain\Model\Livtickerupdate $livtickerupdate
     * @return void
     */
    public function createAction(\Yawave\Yawave\Domain\Model\Livtickerupdate $newLivtickerupdate)
    {

        $this->initializeAction();
        $persistenceManager = $this->objectManager->get(\TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager::class);
        $this->livtickerupdateRepository->add($newLivtickerupdate);
        $persistenceManager->persistAll();
        return $newLivtickerupdate;
    }


    /**
     * action create
     *
     * @param \Yawave\Yawave\Domain\Model\Livtickerpostupdate $livtickerpostupdate
     * @return void
     */
    public function createLiveBlogPostUpdate(\Yawave\Yawave\Domain\Model\Livtickerpostupdate $newLivtickerpostupdate)
    {

        $this->initializeAction();
        $persistenceManager = $this->objectManager->get(\TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager::class);
        $this->livtickerpostupdateRepository->add($newLivtickerpostupdate);
        $persistenceManager->persistAll();
        return $newLivtickerpostupdate;
    }

    /**
     *
     *
     * @param object $liveTickerUpdates
     *
     */
    public function schedulerLivtickerUpdate(object $liveTickerUpdates){
        if(!is_null($this->yawaveConfiguration())){
            $yawaveConfiguration = $this->yawaveConfiguration();

            $yawaveConnection = $this->connectToYawave($yawaveConfiguration);

            if(isset($yawaveConnection['error'])){
                return $yawaveConnection;
            }

            // Application ID get from Arguments
            $applicationId = $yawaveConnection['applicationId'];
            $yawaveClient = $yawaveConnection['client'];
            if(!is_null($yawaveClient) || !is_null($applicationId)){
                $this->yawaveLiveTicker($yawaveClient, $applicationId, $liveTickerUpdates->getFirst());

            }
        }

    }



    /**
     * @param \Yawave\Yawave\Service\YawaveApi $yawave
     * @param string $applicationId
     * @param object $liveTicker
     */
    public function yawaveLiveTicker(\Yawave\Yawave\Service\YawaveApi $yawave, string $applicationId, object $liveTicker){
        // LiveTicker
        if($liveTicker->getEventType() == 'LIVE_BLOG_CREATED') {
            $liveBlog = $yawave->liveblogs($applicationId, $liveTicker->getLiveblogUuid());
            if($liveBlog == ''){
                $notSuccessfulMessage= [
                    'body'=>'live Blog with id - ' . $liveTicker->getLiveblogUuid() . ' return Empty content.',
                    'header'=>'Warning',
                    'error' => 400
                ];
                return $notSuccessfulMessage;
            }
            $this->livtickerupdateRepository->removeUpdate($liveTicker);
            $this->liveblogRepository->createLiveblog($liveBlog);
            return;
        }

        if($liveTicker->getEventType() == 'LIVE_BLOG_UPDATED') {
            $liveBlog = $yawave->liveblogs($applicationId, $liveTicker->getLiveblogUuid());
            if($liveBlog == ''){
                $notSuccessfulMessage= [
                    'body'=>'live Blog with id - ' . $liveTicker->getLiveblogUuid() . ' return Empty content.',
                    'header'=>'Warning',
                    'error' => 400
                ];
                return $notSuccessfulMessage;
            }
            $editLiveBlog = $this->liveblogRepository->findByLiveblogId($liveTicker->getLiveblogUuid())->getFirst();
            $this->livtickerupdateRepository->removeUpdate($liveTicker);
            $this->liveblogRepository->updateLiveblog($editLiveBlog,$liveBlog);
            return;
        }

        if($liveTicker->getEventType() == 'LIVE_BLOG_DELETED') {
            $deleteLiveBlog = $this->liveblogRepository->findByLiveblogId($liveTicker->getLiveblogUuid())->getFirst();
            $this->livtickerupdateRepository->removeUpdate($liveTicker);
            $this->liveblogRepository->delete($deleteLiveBlog);
            return;
        }


        if($liveTicker->getEventType() == 'LIVE_BLOG_POST_CREATED') {
            $livBlog = $this->liveblogRepository->findByLiveblogId($liveTicker->getLiveblogUuid())->getFirst();
            $liveBlogPost = $yawave->liveblogposts($liveTicker->getApplicationUuid(), $liveTicker->getLiveblogUuid(),$liveTicker->getLiveblogPostUuid());
            $status = 1;
            $blogPost = $this->liveblogpostRepository->createLiveblogpost($liveBlogPost,$status);
            $this->livtickerpostupdateRepository->removeUpdate($liveTicker);
            $this->liveblogRepository->addPostToBlog($livBlog,$blogPost);
            return;
        }
        if($liveTicker->getEventType() == 'LIVE_BLOG_POST_UPDATED') {
            $liveBlogPost = $yawave->liveblogposts($liveTicker->getApplicationUuid(), $liveTicker->getLiveblogUuid(),$liveTicker->getLiveblogPostUuid());
            if($liveBlogPost == ''){
                $notSuccessfulMessage= [
                    'body'=>'live Blog Post with id - ' . $liveTicker->getLiveblogUuid() . ' return Empty content.',
                    'header'=>'Warning',
                    'error' => 400
                ];
                return $notSuccessfulMessage;
            }
            $status = 2;
            $editLiveBlogPost = $this->liveblogpostRepository->findByPostId($liveTicker->getLiveblogPostUuid())->getFirst();
            $this->livtickerpostupdateRepository->removeUpdate($liveTicker);
            $this->liveblogpostRepository->updateLiveblogpost($editLiveBlogPost,$liveBlogPost,$status);

            return;
        }

        if($liveTicker->getEventType() == 'LIVE_BLOG_POST_DELETED') {
            $liveBlogPost = $yawave->liveblogposts($liveTicker->getApplicationUuid(), $liveTicker->getLiveblogUuid(),$liveTicker->getLiveblogPostUuid());
            if($liveBlogPost == ''){
                $notSuccessfulMessage= [
                    'body'=>'live Blog Post with id - ' . $liveTicker->getLiveblogUuid() . ' return Empty content.',
                    'header'=>'Warning',
                    'error' => 400
                ];
                return $notSuccessfulMessage;
            }
            $deleteLiveBlogPost = $this->liveblogpostRepository->findByPostId($liveTicker->getLiveblogPostUuid())->getFirst();
            $this->livtickerpostupdateRepository->removeUpdate($liveTicker);
            $this->liveblogpostRepository->delete($deleteLiveBlogPost);
            return;
        }






    }
}