<?php


namespace Yawave\Yawave\Controller;

/***
 *
 * This file is part of the "Yawave Typo3 Extension" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2020 Epicss <info@epicss.io>, Epic-Software-Solution
 *
 ***/

use TYPO3\CMS\Core\Messaging\FlashMessageService;
use Yawave\Yawave\Domain\Model\Backendmodule as Settings;
use TYPO3\CMS\Core\TypoScript\TypoScriptService;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Annotation\Inject;
use Yawave\Yawave\Service\Connection;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager;
use Yawave\Yawave\Service\ErrorMessage;

class BackendmoduleController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * BackendModuleRepository
     *
     * @var \Yawave\Yawave\Domain\Repository\BackendmoduleRepository
     * @Inject
     */
    protected $backendmoduleRepository = NULL;

    /**
     * publicationController
     *
     * @var \Yawave\Yawave\Controller\PublicationsController
     * @Inject
     */
    protected $publicationController = NULL;


    public function initializeExtensionConfigurationAction()
    {

        $settings = $this->backendmoduleRepository->findAll()->getFirst();
        $this->objectManager = GeneralUtility::makeInstance(ObjectManager::class);
        $this->configurationManager = $this->objectManager->get(
            ConfigurationManagerInterface::class
        );
        $curentPageid = (int)GeneralUtility::_GET('id');

        if(!is_null($settings)) {
            $this->configurationManager->setConfiguration(
                [
                    'persistence' => [
                        'storagePid' => $settings->getStoragePid()
                    ],
                ]
            );
        }else{
            $this->configurationManager->setConfiguration(
                [
                    'persistence' => [
                        'storagePid' => $curentPageid
                    ],
                ]
            );
        }

        $this->persistenceManager = $this->objectManager->get(PersistenceManager::class);
    }



    /**
     * Yawave configuration get from extension configuration in InstallTool
     */
    public function yawaveConfiguration(){

        $beConfiguration = $this->backendmoduleRepository->findAll()->getFirst();

        if(!is_null($beConfiguration)) {

            $settings = [
                'client_id' => $clientId = $beConfiguration->getClientId(),
                'secret' => $beConfiguration->getSecret(),
                'token' => $beConfiguration->getHooktoken(),
                'domain' => $beConfiguration->getApiDomain()
            ];
            return $settings;
        }

    }



    public function extensionConfigurationAction(){
        $args = $this->request->getArguments();


        if(!is_null($this->yawaveConfiguration())){
            $yawaveConfiguration = $this->yawaveConfiguration();
        }
        // Get Setting from database
        if(!is_null($this->backendmoduleRepository->findAll())) {
            $settings = $this->backendmoduleRepository->findAll();
        }

        $sitesConfigurations = GeneralUtility::makeInstance(\TYPO3\CMS\Core\Site\SiteFinder::class)->getAllSites();

        // Check if we have instaled Image Magick on the system
        if (!extension_loaded('imagick')) {
            $this->addFlashMessage('Chack configuration for Image Magick', 'Image Magick', \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING);
        }

        if (!extension_loaded('gd')) {
            $this->addFlashMessage('Chack configuration for GD Library', 'GD Library', \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING);
        }

        // Delete all the data from the database
        if(isset($args['emptyTables'])){
            $this->backendmoduleRepository->emptyTables();
            $this->addFlashMessage('The Tables are empty now. You can now run cron to add new data from Yawave', 'Successful delete data from database', \TYPO3\CMS\Core\Messaging\AbstractMessage::OK);
        }



        // Add all the records from Yawave to Typo3
        if(isset($args['addTables'])){

            $yawaveConnection = $this->connectToYawave($yawaveConfiguration);

            // Application ID get from Arguments
            $applicationId = $yawaveConnection['applicationId'];
            $yawaveClient = $yawaveConnection['client'];


            // Synch Tags from Yawave to the Tags model
            $this->publicationController->yawaveSynchTags($yawaveClient, $applicationId);
            $this->addFlashMessage('Tags are now synch with Yawave', 'Successful Insert Tags', \TYPO3\CMS\Core\Messaging\AbstractMessage::OK);

            // Synch Categories and set thear parents from Yawave to the sys_category
            $this->publicationController->yawaveSynchCategories($yawaveClient, $applicationId);
            $this->addFlashMessage('Categories are now synch with Yawave', 'Successful Insert Categories', \TYPO3\CMS\Core\Messaging\AbstractMessage::OK);

            // Synch Metrics
            $this->publicationController->yawaveSynchMetrics($yawaveClient,$applicationId);
            $this->addFlashMessage('Metrics are now synch with Yawave', 'Successful Insert Metrics', \TYPO3\CMS\Core\Messaging\AbstractMessage::OK);

            // Synch Publications and set thear Categories, nad Tags from Yawave to the News
            $this->publicationController->yawaveSynchPublicationsMultilang($yawaveClient, $applicationId);
            $this->addFlashMessage('Publications are now synch with Yawave', 'Successful Insert Publications', \TYPO3\CMS\Core\Messaging\AbstractMessage::OK);

            // Synch Portals and set News to the Portals Model
            $this->publicationController->yawaveSynchPortals($yawaveClient, $applicationId);
            $this->addFlashMessage('Portals are now synch with Yawave', 'Successful Insert Portals', \TYPO3\CMS\Core\Messaging\AbstractMessage::OK);

            $this->addFlashMessage('You can naw go to configurate Yawave on view', 'Successful Insert All records from yawave', \TYPO3\CMS\Core\Messaging\AbstractMessage::OK);

        }


        foreach($sitesConfigurations as $siteConfiguration){
            $languages = $siteConfiguration->getLanguages();
        }

        if(isset($args['errorMessage'])){
            switch ($args['errorMessage']['error']) {
                case 200:
                    ($args['cline']=='clin') ? $this->backendmoduleRepository->emptyTables() : '';
                    $this->addFlashMessage($args['errorMessage']['body'], $args['errorMessage']['header'], \TYPO3\CMS\Core\Messaging\AbstractMessage::OK);
                    break;
                case 400:
                    $this->addFlashMessage($args['errorMessage']['body'], $args['errorMessage']['header'], \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING);
                    break;
                case 401:
                    $this->addFlashMessage($args['errorMessage']['body'], $args['errorMessage']['header'], \TYPO3\CMS\Core\Messaging\AbstractMessage::WARNING);
                    break;
            }
        }

        $i = $settings->count();
        switch ($i) {
            case 0:
                break;
            case 1:
                $this->view->assign('settings', $settings->getFirst());
                break;
        }
        $this->view->assign('active', $args['active']);
        $this->view->assign('languages', $languages);

    }


    /**
     * initialize create action
     *
     * @return void
     */
    public function initializeExtensionSettingsAction() {
        if ($this->arguments->hasArgument('settings')) {
            $this->arguments->getArgument('settings')->getPropertyMappingConfiguration()->allowProperties('languageIds');
            $this->arguments->getArgument('settings')->getPropertyMappingConfiguration()->setTargetTypeForSubProperty('languageIds', 'array');
        }
    }

    /**
     * action extensionSettings
     *
     * @param \Yawave\Yawave\Domain\Model\Backendmodule $settings
     * @return void
     */
    public function extensionSettingsAction(\Yawave\Yawave\Domain\Model\Backendmodule $settings){

        /**
         * @var \TYPO3\CMS\Extbase\Object\ObjectManager $objectManager
         */
        $objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Object\\ObjectManager');

        /**
         * @var \TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager $persistenceManager
         */
        $persistenceManager = $objectManager->get('TYPO3\\CMS\\Extbase\\Persistence\\Generic\\PersistenceManager');

        $exist = $settings->getUid();

        $this->initializeAction();

        ($settings->_isDirty("apiDomain")) ? $cline = 'clin' : $cline = NULL;

        if(is_null($exist)){
            $this->backendmoduleRepository->add($settings);
        }else{
            $this->backendmoduleRepository->update($settings);
        }
        $persistenceManager->persistAll();
        $yawaveConfiguration = $this->yawaveConfiguration();

        // TEST configuration and set message if something is not ok
        $yawaveTestMessage = $this->testYawaveConfiguration($yawaveConfiguration);


        $this->redirect('extensionConfiguration',
            NULL,
            NULL,
            [
                'errorMessage'=>$yawaveTestMessage,
                'active'=> $this->request->getArgument('active'),
                'cline'=> $cline
            ]
        );

    }

    /**
     * action testYawaveConfiguration
     *
     * @param array $settings
     *
     */
    public function testYawaveConfiguration(array $settings){

            $yawave = $this->connectToYawave($settings);

            $successfulMessage = [
                'body'=>'The configuration is set properly.',
                'header'=>'Configuration',
                'error' => 200
            ];

            $result = isset($yawave['error']) ?  $yawave : $successfulMessage;


            return $result;


    }



    /**
     * Yawave connection and get applicationId and client
     *
     * @param array $settings
     */
    public function connectToYawave(array $settings){

        if(!empty($settings['client_id']) and !empty($settings['secret']) and !empty($settings['token']) and !empty($settings['domain'])) {

            $yawave = (new Connection())->apiConnection($settings['client_id'],$settings['secret'],$settings['domain']);

            return $yawave;
        }

        $notSuccessfulMessage= [
            'body'=>'The configuration is not set properly. Check Client Id and Secret',
            'header'=>'Configuration',
            'error' => 400
        ];
        return $notSuccessfulMessage;

    }

}