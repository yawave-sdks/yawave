<?php


namespace Yawave\Yawave\Service;


class YawaveRemapLagnuage
{
    /**
     * @param array $records
     * @return array
     */
    public function sortByLanguage(array $records) {

        $languages = [];
        foreach ($records as $record){
            $languages = array_merge($languages,$record['languages']);
        }
        $languages = array_unique($languages);

        $sortedRecords = [];

        foreach ($languages as $wantedLanguage){

            $languageRecords = $records;
            foreach ($languageRecords as $key => &$record){
                if(!in_array($wantedLanguage,$record['languages'])){
                    unset($languageRecords[$key]);
                    continue;
                }
                $record = $this->parseForLanguage($record,$wantedLanguage);
            }
            if($wantedLanguage == 'zh-hans'){
                $wantedLanguage = 'zh';
            }
            $sortedRecords[$wantedLanguage] = $languageRecords;

        }

        return $sortedRecords;



    }


    private function parseForLanguage($var,$wantedLanguage){

        if(!is_array($var)){
            return $var;
        }

        if(array_key_exists($wantedLanguage,$var)){
            return $var[$wantedLanguage];
        }

        foreach($var as  &$item){
            $item = $this->parseForLanguage($item, $wantedLanguage);
        }

        return $var;

    }

}
