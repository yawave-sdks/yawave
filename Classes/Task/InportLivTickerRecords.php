<?php


namespace Yawave\Yawave\Task;

use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use Yawave\Yawave\Service\ErrorMessage;

class InportLivTickerRecords extends \TYPO3\CMS\Scheduler\Task\AbstractTask
{

    /**
     * @var \TYPO3\CMS\Extbase\Object\ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * @param \TYPO3\CMS\Extbase\Object\ObjectManagerInterface $objectManager
     */
    public function injectObjectManager(\TYPO3\CMS\Extbase\Object\ObjectManagerInterface $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    public function execute() {

        $objectManager = GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\Object\ObjectManager::class);

        $liveTickerUpdateRepository = $objectManager->get(\Yawave\Yawave\Domain\Repository\LivtickerupdateRepository::class);
        $liveTickerBlogPostUpdateRepository = $objectManager->get(\Yawave\Yawave\Domain\Repository\LivtickerpostupdateRepository::class);
        $conntrollerLiveTicker = $objectManager->get(\Yawave\Yawave\Controller\LiveTickerController::class);

        $updatesBlog = $liveTickerUpdateRepository->findAll();
        $updatesBlogPosts = $liveTickerBlogPostUpdateRepository->findAll();

        if(count($updatesBlog) > 0){
            return $conntrollerLiveTicker->schedulerLivtickerUpdate($updatesBlog);
        }
        if(count($updatesBlogPosts) > 0){
            return $conntrollerLiveTicker->schedulerLivtickerUpdate($updatesBlogPosts);
        }
        return true;

    }

}