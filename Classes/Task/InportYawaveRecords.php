<?php

namespace Yawave\Yawave\Task;

use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use Yawave\Yawave\Service\ErrorMessage;

class InportYawaveRecords extends \TYPO3\CMS\Scheduler\Task\AbstractTask
{
    /**
     * @var \TYPO3\CMS\Extbase\Object\ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * @param \TYPO3\CMS\Extbase\Object\ObjectManagerInterface $objectManager
     */
    public function injectObjectManager(\TYPO3\CMS\Extbase\Object\ObjectManagerInterface $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    public function execute() {
        $objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\\CMS\Extbase\\Object\\ObjectManager');
        $configurationManager = $objectManager->get('TYPO3\\CMS\\Extbase\\Configuration\\ConfigurationManager');
        $extbaseFrameworkConfiguration = $configurationManager->getConfiguration(\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_FULL_TYPOSCRIPT);
        $configuratedlanguages = $extbaseFrameworkConfiguration['plugin.']['tx_news.']['settings.']['language.'];
        if(is_null($configuratedlanguages)){
            $body = "You didn't set languages id-s in typoscript.";
            $header = 'Configuration';
            return (new ErrorMessage())->showError($header,$body);
        }
        $objectManager = GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\Object\ObjectManager::class);
        $conntrollerPublication = $objectManager->get(\Yawave\Yawave\Controller\PublicationsController::class);
        $updateRepository = $objectManager->get(\Yawave\Yawave\Domain\Repository\UpdateRepository::class);
        $publicationsRepository = $objectManager->get(\Yawave\Yawave\Domain\Repository\PublicationsRepository::class);
        $metricsRepository = $objectManager->get(\Yawave\Yawave\Domain\Repository\MetricsRepository::class);
        $metrics = $metricsRepository->findAll();
        $updates = $updateRepository->findAll();
        $publications = $publicationsRepository->findAll();
        $yawave = $conntrollerPublication->connectToYawave();
        $applicationId = $yawave['applicationId'];
        $yawaveClient = $yawave['client'];


        if(count($publications) == 0) {
            $conntrollerPublication->yawaveSynch($yawave);
            \TYPO3\CMS\Core\Utility\GeneralUtility::trimExplode(',', strtolower('cacheTag:tx_yawave'), true);
        }

        if(count($metrics) > 0) {
            $conntrollerPublication->yawaveSynchMetrics($yawaveClient,$applicationId);
            \TYPO3\CMS\Core\Utility\GeneralUtility::trimExplode(',', strtolower('cacheTag:tx_yawave'), true);
        }

        if(count($updates) > 0){
            $conntrollerPublication->schedulerUpdatePublication($updates);
            \TYPO3\CMS\Core\Utility\GeneralUtility::trimExplode(',', strtolower('cacheTag:tx_yawave'), true);
        }

        return true;

    }

}
