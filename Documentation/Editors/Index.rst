.. include:: ../../Includes.txt


Editors
==============

Intro
------------

Yawave provides you with many possibilities to create publications, portals and tools (form, subscribe, share, payment, engage...) which can be integrated into your website.

The integration of these content usually happens when a specific javascript code snippet is added to your website. The yawave extension simplifies this task. After the extension is successfully installed and configured, every TYPO3 editor is able to add existing yawave content to the website simply by publishing it in the yawave application.
For this how-to we are assuming that you are already comfortable working with yawave and TYPO3 as an editor and know your way around the TYPO3 backend.


Add publications
----------------

New publications can be added by adding a new system plugin content element.

.. image:: ../Images/image11.png

The “list view” can be used for creating an overview page of publication cover elements and the “detail view” for creating publication detail views.
Data will show up according to the filter settings of the news system (categories, tags, portals...)
Publications which match the news system filter will be displayed few seconds after the publications has been published/republished in the yawave application.

Add portals
------------

Portals can be added as a filter criterium additional to categories and/or tags. In the yawave application there is a special portal view for easier publication handling.


Add tools
----------

Adding tools is exactly like adding any other content element. You have to add the needed tools in the yawave application to the specific publication. After publishing this change, tools will appear on your TYPO3 Frontend as “Actionbuttons” in the publication header (detailview).