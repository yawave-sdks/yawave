.. include:: ../../Includes.txt


Administration
==============

Intro
------------

This document takes you step by step through the installation of the yawave typo3 extension. It does not focus on specific operating systems.

The yawave Typo3 Extension can be found here https://extensions.typo3.org/extension/yawave/

You can find the documentation for yawave editors here https://bitbucket.org/yawave-sdks/yawave/src/master/Documentation/Editors/Index.rst



Installation
------------

The extension needs to be installed as any other extension of TYPO3 CMS:

#. Switch to the module “Extension Manager”.

#. Get the extension

   #. **Get it from the Extension Manager:** Press the “Retrieve/Update”
      button and search for the extension key *hubspot* and import the
      extension from the repository.

   #. **Get it from typo3.org:** You can always get a current version from
      `https://extensions.typo3.org/extension/yawave
      <https://extensions.typo3.org/extension/yawave>`_ by
      the zip version. Upload
      the file afterwards in the Extension Manager.

   #. **Use composer**: Use `composer require yawave-sdks/yawave`.

Setup TYPO3 extension via composer
-----------------------------------

The recommended way of installing yawave TYPO3 Extension is via composer.
If you do not want to use composer, follow the package installation guide.

Setup TYPO3 without composer (package installation guide)
---------------------------------------------------------

Install dependent extensions
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    #. Install the “news system” extension before you install the “yawave” extension +
        .. image:: ../Images/image1.png

    #. Install the “scheduler ” extension before you install the “yawave” extension.

    #. Install the “yawave” extension.

Set StoragePid
^^^^^^^^^^^^^^

    #. Change StoragePid in “ext_typoscript_setup.typoscript” according to storagefolder id.
       Path: …ext/yawave
        .. image:: ../Images/image3.png
    #. Persist StoragePid in site template config. Example:
    
.. code-block:: typoscript
    
    config.tx_extbase.persistence.storagePid = 2

Setup Languages
^^^^^^^^^^^^^^^

    #. Configure the same languages on your Typo3 environment as they are used in your yawave application.
       List website language, sites language config,
        .. image:: ../Images/image2.png
        .. image:: ../Images/image5.png
        .. image:: ../Images/image4.png
        .. image:: ../Images/image7.png
        .. image:: ../Images/image6.png
    #. Set fallback type
        .. image:: ../Images/image9.png
    #. Setup site template and add id’s of languages. Example:
        .. image:: ../Images/image8.png


Example:

.. code-block:: typoscript

   plugin.tx_news{
        settings {
            list {
                paginate {
                    insertAbove = 0
                    insertBelow = 1
                }
            }
            language {
                en = 0
                de = 1
                it = 2
                zh = 3
            }
        }
    }
    config.tx_extbase.persistence.storagePid = 2



Setup hook for yawave Typo3 extension in config.yaml
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    #. In the config.yaml file you need to set the hook for the yawave extension.
       This file can be found under path **typo3conf > site > (website_name) > config.yaml
       (need to be done on Host not through CMS).**

Example:

.. code-block:: yaml

    PageTypeSuffix:
        type: PageType
        default: ''
        map:
            yawavehook: 9999

Configure scheduler
^^^^^^^^^^^^^^^^^^^

    #. Add scheduler task of class “First insert of all records from yawave”
       Type “Recurring” and wished “Frequency of task”.
       For example: */5 * * * * (all 5 minutes),
       don’t enable automatic run yet (do it after all other steps have been done).
       .. image:: ../Images/image12.png
       .. image:: ../Images/image10.png

Add content elements
^^^^^^^^^^^^^^^^^^^^

    #. Pull yawave content by running scheduler once

    #. Add news content (do it for every language)
    .. image:: ../Images/image11.png

    #. Choose filter according to your needs (portal, tags, category…) and save new page content
    .. image:: ../Images/image13.png

    #. Add all languages in pages (detail and overview pages)

    #. Choose layout

    #. Initiate automated scheduler now


How to get your yawave application id?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

In your yawave-account you can find in the top-navigation
an icon with 9 little boxes. click on it and choose the yawave application
you wanna have the application id for.
now you can mouseover the three dots and click on "copy yawave application id".


How to get your app id and app secret?
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

first you need to add a Typo3 connection to your yawave account:
Integrate -> Connections -> Add connection
After creation you can find app id and app secret in the connection details .


Latest version from git
^^^^^^^^^^^^^^^^^^^^^^^

You can get the latest version from git by using the git command:
git clone git@bitbucket.org:yawave-sdks/yawave.git

