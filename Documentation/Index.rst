
.. include:: Includes.txt

.. _start:

=============================================================
Yawave
=============================================================
.. _Start:


:Author: Simon Baumgartner <simon.baumgartner@yawave.com>
:License:
:Company:  yawave ag
:Description:
   The yawave extension for TYPO3 provides an easy solution for adding yawave publications, portals and tools directly to your TYPO3 Environment.

.. toctree::
   :maxdepth: 2
   :glob:

    Administrators/Index
    Editors/Index
    Changelog/Index
