<?php

/***************************************************************
 * Extension Manager/Repository config file for ext: "yawave"
 *
 * Auto generated by Extension Builder 2020-09-25
 *
 * Manual updates:
 * Only the data in the array - anything else is removed by next write.
 * "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = [
    'title' => 'Yawave Typo3 Extension',
    'description' => '',
    'category' => 'plugin',
    'author' => 'Yawave',
    'author_email' => 'simon.baumgartner@yawave.com',
    'state' => 'stable',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearCacheOnLoad' => 0,
    'version' => '1.0.15',
    'constraints' => [
        'depends' => [
            'typo3' => '9.5.17-10.9.99',
            'news' => '8.0.0-8.9.99',
            'scheduler' => '9.5.17-10.9.99',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
