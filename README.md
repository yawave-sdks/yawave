# Yawave

## 1 Features

* Based on extbase & fluid and news extension, implementing best practices from TYPO3 CMS

* [Comprehensive documentation][1]

## 2 Usage

### 2.1 Installation

#### Installation using Composer

The recommended way to install the extension is using [Composer][2].

Run the following command within your Composer based TYPO3 project:

```
composer require yawave-sdks/yawave
```

#### Installation as extension from TYPO3 Extension Repository (TER)

Download and install the [extension][3] with the extension manager module.

### 2.2 Minimal setup

1. Install the “News system” extension before you install the “yawave” extension.
2. Install the “Scheduler ” extension before you install the “yawave” extension .
3. Install the “yawave” extension. 
4. Change StoragePid in “ext_typoscript_setup.typoscript” according to Storagefolder ID Path: …ext/yawave.
5. Configure the same languages on your Typo3 environment as they are used in your yawave application.
6. Set Fallback Type.
7. Setup site template and add id’s of languages.
8. Setup Hook for yawave extension in config.yaml.
9. Configure Scheduler.
10. Pull yawave content by running scheduler once.
11. Add News Content.
12. Choose filter according to your needs (portal, tags, category…) and save new page content.
13. Add all Languages in Pages.
14. Choose Layout.
15. Initiate automated Scheduler now.


[1]: https://bitbucket.org/yawave-sdks/yawave/
[2]: https://getcomposer.org/
[3]: https://extensions.typo3.org/extension/yawave/
