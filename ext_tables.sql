#
# Table structure for table 'tx_news_domain_model_news'
#
CREATE TABLE tx_news_domain_model_news (

	yawave_publication_id varchar(255) DEFAULT '0' NOT NULL,
	news_type varchar(255) DEFAULT '' NOT NULL,
	url varchar(255) DEFAULT '',
	page_height varchar(255) DEFAULT '' NOT NULL,
	cover int(11) unsigned DEFAULT '0',
	metric int(11) unsigned DEFAULT '0',
	main_category int(11) DEFAULT '0' NOT NULL,
	image int(11) unsigned DEFAULT '0',
	header int(11) unsigned DEFAULT '0',
	tool int(11) unsigned DEFAULT '0' NOT NULL,

);


#
# Table structure for table 'tx_yawave_domain_model_tools'
#
CREATE TABLE tx_yawave_domain_model_tools (

	publications int(11) unsigned DEFAULT '0' NOT NULL,

	yawave_tools_id varchar(255) DEFAULT '0' NOT NULL,
	tool_type varchar(255) DEFAULT '' NOT NULL,
	tool_label varchar(255) DEFAULT '' NOT NULL,
	icon int(11) unsigned DEFAULT '0' NOT NULL,
	reference int(11) unsigned DEFAULT '0' NOT NULL,

);

#
# Table structure for table 'tx_yawave_domain_model_icons'
#
CREATE TABLE tx_yawave_domain_model_icons (

	tools int(11) unsigned DEFAULT '0' NOT NULL,

	source varchar(255) DEFAULT '' NOT NULL,
	name varchar(255) DEFAULT '' NOT NULL,
	type varchar(255) DEFAULT '' NOT NULL,

);


#
# Table structure for table 'tx_yawave_domain_model_backendmodule'
#
CREATE TABLE tx_yawave_domain_model_backendmodule (

    hooktoken varchar(255) DEFAULT '' NOT NULL,
    client_id varchar(255) DEFAULT '' NOT NULL,
    storage_pid varchar(255) DEFAULT '' NOT NULL,
    secret varchar(255) DEFAULT '' NOT NULL,
    api_domain varchar(255) DEFAULT '' NOT NULL,
    language_ids varchar(255) DEFAULT '' NOT NULL,

);


#
# Table structure for table 'tx_yawave_domain_model_references'
#
CREATE TABLE tx_yawave_domain_model_references (

	tools int(11) unsigned DEFAULT '0' NOT NULL,

	link_url varchar(255) DEFAULT '',

);

#
# Table structure for table 'tx_yawave_domain_model_tools'
#
CREATE TABLE tx_yawave_domain_model_tools (

	publications int(11) unsigned DEFAULT '0' NOT NULL,

);


#
# Table structure for table 'tx_yawave_domain_model_relatedcontent'
#
CREATE TABLE tx_yawave_domain_model_relatedcontent (

	title varchar(255) DEFAULT '' NOT NULL,

);


#
# Table structure for table 'tx_yawave_domain_model_images'
#
CREATE TABLE tx_yawave_domain_model_images (

    url varchar(255) DEFAULT '' NOT NULL,
    image int(11) unsigned DEFAULT '0',
	title varchar(255) DEFAULT '' NOT NULL,
	width varchar(255) DEFAULT '',
	height varchar(255) DEFAULT '',
	focus_x varchar(255) DEFAULT '',
	focus_y varchar(255) DEFAULT '',

);

#
# Table structure for table 'tx_yawave_domain_model_covers'
#
CREATE TABLE tx_yawave_domain_model_covers (

	title varchar(255) DEFAULT '' NOT NULL,
	title_image int(11) unsigned DEFAULT '0' NOT NULL,
	description text,
	images int(11) unsigned DEFAULT '0' NOT NULL,
    sys_language_uid int(11) DEFAULT '0' NOT NULL,
    l10n_parent int(11) DEFAULT '0' NOT NULL,
    l10n_diffsource mediumtext,
    l10n_source int(11) DEFAULT '0' NOT NULL,

);

#
# Table structure for table 'tx_yawave_domain_model_metrics'
#
CREATE TABLE tx_yawave_domain_model_metrics (

	yawave_publication_id varchar(255) DEFAULT '0' NOT NULL,
	views varchar(255) DEFAULT '' NOT NULL,
	recipients varchar(255) DEFAULT '' NOT NULL,
	engagements varchar(255) DEFAULT '' NOT NULL,

);

#
# Table structure for table 'tx_yawave_domain_model_portals'
#
CREATE TABLE tx_yawave_domain_model_portals (

	yawave_portal_id varchar(255) DEFAULT '0' NOT NULL,
	title varchar(255) DEFAULT '' NOT NULL,
	description text,
	publications int(11) unsigned DEFAULT '0' NOT NULL,
	images int(11) unsigned DEFAULT '0' NOT NULL,
	releted_content int(11) unsigned DEFAULT '0' NOT NULL,

);

#
# Table structure for table 'tx_yawave_domain_model_update'
#
CREATE TABLE tx_yawave_domain_model_update (

	publication_uuid varchar(255) DEFAULT '' NOT NULL,
	status varchar(255) DEFAULT '' NOT NULL,

);

#
# Table structure for table 'tx_yawave_domain_model_livtickerupdate'
#
CREATE TABLE tx_yawave_domain_model_livtickerupdate (

    event_type varchar(255) DEFAULT '' NOT NULL,
    application_uuid varchar(255) DEFAULT '' NOT NULL,
    liveblog_uuid varchar(255) DEFAULT '' NOT NULL,

);

#
# Table structure for table 'tx_yawave_domain_model_livtickerpostupdate'
#
CREATE TABLE tx_yawave_domain_model_livtickerpostupdate (

    event_type varchar(255) DEFAULT '' NOT NULL,
    application_uuid varchar(255) DEFAULT '' NOT NULL,
    liveblog_uuid varchar(255) DEFAULT '' NOT NULL,
    liveblog_post_uuid varchar(255) DEFAULT '' NOT NULL,

);

#
# Table structure for table 'tx_yawave_domain_model_liveblog'
#
CREATE TABLE tx_yawave_domain_model_liveblog (
    liveblog_id varchar(255) DEFAULT '' NOT NULL,
    title varchar(255) DEFAULT '' NOT NULL,
    description text,
    images int(11) unsigned DEFAULT '0' NOT NULL,
    liveblog_category_id varchar(100) DEFAULT '' NOT NULL,
    yawave_tag_id varchar(255) DEFAULT '' NOT NULL,
    type varchar(255) DEFAULT '' NOT NULL,
    status varchar(255) DEFAULT '' NOT NULL,
    location varchar(255) DEFAULT '' NOT NULL,
    start_date int(11) DEFAULT '0' NOT NULL,
    home_competitor varchar(100) DEFAULT '' NOT NULL,
    away_competitor varchar(100) DEFAULT '' NOT NULL,
    sources varchar(100) DEFAULT '' NOT NULL,
    post varchar(100) DEFAULT '' NOT NULL,
    sport_event_id varchar(100) DEFAULT '' NOT NULL,
);

#
# Table structure for table 'tx_yawave_domain_model_liveblogpost'
#
CREATE TABLE tx_yawave_domain_model_liveblogpost (
    post_id varchar(255) DEFAULT '' NOT NULL,
    source varchar(255) DEFAULT '' NOT NULL,
    periods varchar(255) DEFAULT '0',
    minute int(11) DEFAULT '0',
    status int(11) DEFAULT '0',
    title varchar(255) DEFAULT '' NOT NULL,
    text text,
    url text,
    publication_id varchar(255) DEFAULT '0',
    embed_code text,
    pinned int(11) DEFAULT '0',
    creation_date int(11) DEFAULT '0' NOT NULL,
    timeline_timestamp int(11) DEFAULT '0',
    action_id varchar(255) DEFAULT '0',
    person_id varchar(255) DEFAULT '0',
    action varchar(255) DEFAULT '0',
    person varchar(255) DEFAULT '0',
    external_id varchar(255) DEFAULT '' NOT NULL,
    type varchar(255) DEFAULT '' NOT NULL,
    stoppage_time int(11) DEFAULT '0',
    match_clock varchar(255) DEFAULT '0',
    competitor varchar(100) DEFAULT '0',
    players varchar(100) DEFAULT '0',
    home_score varchar(11) DEFAULT '' NOT NULL,
    away_score varchar(11) DEFAULT '' NOT NULL,
    injury_time varchar(100) DEFAULT '' NOT NULL,
    blog varchar(100) DEFAULT '' NOT NULL,
);


#
# Table structure for table 'tx_yawave_domain_model_players'
#
CREATE TABLE tx_yawave_domain_model_players (
    external_id varchar(100) DEFAULT '' NOT NULL,
    name varchar(255) DEFAULT '' NOT NULL,
    type varchar(255) DEFAULT '' NOT NULL,
);

#
# Table structure for table 'tx_yawave_domain_model_competitor'
#
CREATE TABLE tx_yawave_domain_model_competitor (
    external_id varchar(100) DEFAULT '' NOT NULL,
    name varchar(255) DEFAULT '' NOT NULL,
);


#
# Table structure for table 'tx_yawave_liveblogpost_players_mm'
#
CREATE TABLE tx_yawave_liveblogpost_players_mm (
   uid_local int(11) unsigned DEFAULT '0' NOT NULL,
   uid_foreign int(11) unsigned DEFAULT '0' NOT NULL,
   sorting int(11) unsigned DEFAULT '0' NOT NULL,
   sorting_foreign int(11) unsigned DEFAULT '0' NOT NULL,

   PRIMARY KEY (uid_local,uid_foreign),
   KEY uid_local (uid_local),
   KEY uid_foreign (uid_foreign)
);


#
# Table structure for table 'tx_yawave_domain_model_homecompetitor'
#
CREATE TABLE tx_yawave_domain_model_homecompetitor (
    name varchar(255) DEFAULT '' NOT NULL,
    images int(11) unsigned DEFAULT '0' NOT NULL,
);


#
# Table structure for table 'tx_yawave_domain_model_awaycompetitor'
#
CREATE TABLE tx_yawave_domain_model_awaycompetitor (
    name varchar(255) DEFAULT '' NOT NULL,
    images int(11) unsigned DEFAULT '0' NOT NULL,
);



#
# Table structure for table 'tx_yawave_liveblog_sources_mm'
#
CREATE TABLE tx_yawave_liveblog_sources_mm (
   uid_local int(11) unsigned DEFAULT '0' NOT NULL,
   uid_foreign int(11) unsigned DEFAULT '0' NOT NULL,
   sorting int(11) unsigned DEFAULT '0' NOT NULL,
   sorting_foreign int(11) unsigned DEFAULT '0' NOT NULL,

   PRIMARY KEY (uid_local,uid_foreign),
   KEY uid_local (uid_local),
   KEY uid_foreign (uid_foreign)
);



#
# Table structure for table 'tx_yawave_domain_model_homecompetitorexternal'
#
CREATE TABLE tx_yawave_domain_model_homecompetitorexternal (
    name varchar(255) DEFAULT '' NOT NULL,
    external_id varchar(100) DEFAULT '' NOT NULL,
);

#
# Table structure for table 'tx_yawave_domain_model_awaycompetitorexternal'
#
CREATE TABLE tx_yawave_domain_model_awaycompetitorexternal (
    name varchar(255) DEFAULT '' NOT NULL,
    external_id varchar(100) DEFAULT '' NOT NULL,
);

#
# Table structure for table 'tx_yawave_domain_model_sources'
#
CREATE TABLE tx_yawave_domain_model_sources (
    type varchar(255) DEFAULT '' NOT NULL,
    sport_event_id varchar(100) DEFAULT '' NOT NULL,
    competition varchar(100) DEFAULT '' NOT NULL,
    home_competitor_external varchar(100) DEFAULT '' NOT NULL,
    away_competitor_external varchar(100) DEFAULT '' NOT NULL,
);


#
# Table structure for table 'tx_yawave_domain_model_competition'
#
CREATE TABLE tx_yawave_domain_model_competition (
    external_id varchar(100) DEFAULT '' NOT NULL,
    name varchar(100) DEFAULT '' NOT NULL,
);


#
# Table structure for table 'tx_news_domain_model_tag'
#
CREATE TABLE tx_news_domain_model_tag (

	yawave_tag_id varchar(255) DEFAULT '0' NOT NULL,

);

#
# Table structure for table 'sys_category'
#
CREATE TABLE sys_category (

	yawave_category_id varchar(255) DEFAULT '0' NOT NULL,
	yawave_category_parent_id varchar(255) DEFAULT '' NOT NULL,

);


#
# Table structure for table 'tx_yawave_domain_model_headers'
#
CREATE TABLE tx_yawave_domain_model_headers (

	title varchar(255) DEFAULT '' NOT NULL,
	description text,
	image int(11) unsigned DEFAULT '0' NOT NULL,
    sys_language_uid int(11) DEFAULT '0' NOT NULL,
    l10n_parent int(11) DEFAULT '0' NOT NULL,
    l10n_diffsource mediumtext,
    l10n_source int(11) DEFAULT '0' NOT NULL,

);




#
# Table structure for table 'tx_yawave_covers_images_mm'
#
CREATE TABLE tx_yawave_covers_images_mm (
	uid_local int(11) unsigned DEFAULT '0' NOT NULL,
	uid_foreign int(11) unsigned DEFAULT '0' NOT NULL,
	sorting int(11) unsigned DEFAULT '0' NOT NULL,
	sorting_foreign int(11) unsigned DEFAULT '0' NOT NULL,

	PRIMARY KEY (uid_local,uid_foreign),
	KEY uid_local (uid_local),
	KEY uid_foreign (uid_foreign)
);

#
# Table structure for table 'tx_yawave_covers_title_image_mm'
#
CREATE TABLE tx_yawave_covers_title_image_mm (
	uid_local int(11) unsigned DEFAULT '0' NOT NULL,
	uid_foreign int(11) unsigned DEFAULT '0' NOT NULL,
	sorting int(11) unsigned DEFAULT '0' NOT NULL,
	sorting_foreign int(11) unsigned DEFAULT '0' NOT NULL,

	PRIMARY KEY (uid_local,uid_foreign),
	KEY uid_local (uid_local),
	KEY uid_foreign (uid_foreign)
);

#
# Table structure for table 'tx_yawave_portals_publications_mm'
#
CREATE TABLE tx_yawave_portals_publications_mm (
	uid_local int(11) unsigned DEFAULT '0' NOT NULL,
	uid_foreign int(11) unsigned DEFAULT '0' NOT NULL,
	sorting int(11) unsigned DEFAULT '0' NOT NULL,
	sorting_foreign int(11) unsigned DEFAULT '0' NOT NULL,

	PRIMARY KEY (uid_local,uid_foreign),
	KEY uid_local (uid_local),
	KEY uid_foreign (uid_foreign)
);

#
# Table structure for table 'tx_yawave_portals_relatedcontent_mm'
#
CREATE TABLE tx_yawave_portals_relatedcontent_mm (
	uid_local int(11) unsigned DEFAULT '0' NOT NULL,
	uid_foreign int(11) unsigned DEFAULT '0' NOT NULL,
	sorting int(11) unsigned DEFAULT '0' NOT NULL,
	sorting_foreign int(11) unsigned DEFAULT '0' NOT NULL,

	PRIMARY KEY (uid_local,uid_foreign),
	KEY uid_local (uid_local),
	KEY uid_foreign (uid_foreign)
);

#
# Table structure for table 'tx_yawave_portals_images_mm'
#
CREATE TABLE tx_yawave_portals_images_mm (
	uid_local int(11) unsigned DEFAULT '0' NOT NULL,
	uid_foreign int(11) unsigned DEFAULT '0' NOT NULL,
	sorting int(11) unsigned DEFAULT '0' NOT NULL,
	sorting_foreign int(11) unsigned DEFAULT '0' NOT NULL,

	PRIMARY KEY (uid_local,uid_foreign),
	KEY uid_local (uid_local),
	KEY uid_foreign (uid_foreign)
);


#
# Table structure for table 'tx_yawave_domain_model_images'
#
CREATE TABLE tx_yawave_domain_model_images (

	headers int(11) unsigned DEFAULT '0' NOT NULL,

);

#
# Table structure for table 'tx_yawave_domain_model_icons'
#
CREATE TABLE tx_yawave_domain_model_icons (

	tools int(11) unsigned DEFAULT '0' NOT NULL,

);

#
# Table structure for table 'tx_yawave_domain_model_references'
#
CREATE TABLE tx_yawave_domain_model_references (

	tools int(11) unsigned DEFAULT '0' NOT NULL,

);